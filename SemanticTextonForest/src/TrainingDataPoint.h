/*
 * TrainingDataPoint.h
 *
 *  Created on: 11 Oct 2012
 *      Author: Aly
 */

#ifndef TRAININGDATAPOINT_H_
#define TRAININGDATAPOINT_H_

#include <string>
#include "ImagePoint.h"
#include "STFDataPoint.h"

class TrainingDataPoint : public STFDataPoint{
private:
	int row;
	int col;
	int class_label;
	int patch_top_left_row;
	int patch_top_left_col;
	int patch_size_rows;
	int patch_size_cols;
	int image_hash_key;
	std::string image_filename;
	double class_weight;
public:
	TrainingDataPoint(int row, int col, int class_label, int patch_top_left_row, 
			int patch_top_left_col, int patch_size_rows, int patch_size_cols, int image_hash_key, std::string image_filename);
	
	int get_class_label() const;

	inline bool operator==(const TrainingDataPoint& other) const{
		return other.class_label == this->class_label;
	}
	inline bool operator!=(const TrainingDataPoint& other) const{
		return !(*this == other);
	}

	void set_class_weight(double d);
	double get_class_weight();
	
	virtual int get_image_hash_key() const;
	virtual int get_patch_top_left_row() const;
	virtual int get_patch_top_left_col() const;
	virtual int get_patch_size_rows() const;
	virtual int get_patch_size_cols() const;
	virtual int get_row() const;
	virtual int get_col() const;
	virtual std::string get_image_filename() const;
};

#endif /* TRAININGDATAPOINT_H_ */
