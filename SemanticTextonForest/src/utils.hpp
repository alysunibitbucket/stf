/*
 * utils.hpp
 *
 *  Created on: 11 Oct 2012
 *      Author: Aly
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <vector>
#include <string>

template <typename T> T get_random_item(const std::vector<T>& v){
	int index = rand()%v.size();
	return v.at(index);
}


template <typename T> T get_and_remove_random_item(std::vector<T>& v){
	//not perfectley uniformly distributed by using % but fast and as we dont need the perfect distribution for this task it will do
	int index = rand()%v.size();
	T elem = v.at(index);
	remove_at(v, index);
	return elem;
}

template <typename T> void remove_at(std::vector<T>& v, typename std::vector<T>::size_type index){
	std::swap(v[index], v.back());
	v.pop_back();
}

template <typename T> bool contains(std::vector<T>& v, T obj){
	return std::find(v.begin(), v.end(), obj) != v.end();
}

bool replace(std::string& str, const std::string& from, const std::string& to);

int get_random_number_in_range(int min, int max);


#endif /* UTILS_HPP_ */
