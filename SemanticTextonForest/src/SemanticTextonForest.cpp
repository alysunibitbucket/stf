/*
 * SemanticTextonForest.cpp
 *
 *  Created on: 12 Oct 2012
 *      Author: Aly
 */

#include "SemanticTextonForest.h"
#include <boost/filesystem.hpp>
#include <vector>
#include <string>
#include <boost/unordered_map.hpp>
#include <opencv2/opencv.hpp>
#include "RGB.h"
#include "TrainingDataPoint.h"
#include "ImageSampler.h"
#include "utils.hpp"
#include "TrainingDataPointGenerator.h"
#include "RandomTree.h"
#include <math.h>
#include "boost/timer/timer.hpp"
#include "boost/multi_array.hpp"

using namespace boost;
using namespace boost::filesystem;
using namespace std;
using namespace cv;
using namespace math;

double value_at_random_pixel_and_channel(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_value, vector<Mat>* preloaded_images, int* random_values);
double multi_channel_random_subtract_abs(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_value, vector<Mat>* preloaded_images, int* random_values);
double single_channel_subtract_abs(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_value, vector<Mat>* preloaded_images, int* random_values);
double multi_channel_random_add(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_value, vector<Mat>* preloaded_images, int* random_values);
double single_channel_add(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_value, vector<Mat>* preloaded_images, int* random_values);

int* value_at_random_pixel_random_parameter_generator(int patch_size_rows, int patch_size_cols){
	int vrc_first_random_row = get_random_number_in_range(0, patch_size_rows - 1);
	int vrc_first_random_col = get_random_number_in_range(0, patch_size_cols - 1);
	int vrc_channel = get_random_number_in_range(0,2);
		
	int* random_values = new int[3];
	random_values[0] = vrc_first_random_row;
	random_values[1] = vrc_first_random_col;
	random_values[2] = vrc_channel;
	return random_values;
}
int* multi_channel_random_subtract_abs_random_parameter_generator(int patch_size_rows, int patch_size_cols){
	int mcrsa_first_random_row = get_random_number_in_range(0, patch_size_rows - 1);
	int mcrsa_second_random_row = get_random_number_in_range(0, patch_size_rows - 1);
	int mcrsa_first_random_col = get_random_number_in_range(0, patch_size_cols - 1);
	int mcrsa_second_random_col = get_random_number_in_range(0, patch_size_cols - 1);
	int mcrsa_channel_one = get_random_number_in_range(0,2);
	int mcrsa_channel_two = get_random_number_in_range(0,2);
		
	int* random_values = new int[6];
	random_values[0] = mcrsa_first_random_row;
	random_values[1] = mcrsa_first_random_col;
	random_values[2] = mcrsa_second_random_row;
	random_values[3] = mcrsa_second_random_col;
	random_values[4] = mcrsa_channel_one;
	random_values[5] = mcrsa_channel_two;
	
	return random_values;
}
int* single_channel_subtract_abs_random_parameter_generator(int patch_size_rows, int patch_size_cols){
	int scsa_first_random_row = get_random_number_in_range(0, patch_size_rows - 1);
	int scsa_second_random_row = get_random_number_in_range(0, patch_size_rows - 1);
	int scsa_first_random_col = get_random_number_in_range(0, patch_size_cols - 1);
	int scsa_second_random_col = get_random_number_in_range(0, patch_size_cols - 1);
	int scsa_channel = get_random_number_in_range(0,2);
		
	int* scsa_random_values = new int[5];
	scsa_random_values[0] = scsa_first_random_row;
	scsa_random_values[1] = scsa_first_random_col;
	scsa_random_values[2] = scsa_second_random_row;
	scsa_random_values[3] = scsa_second_random_col;
	scsa_random_values[4] = scsa_channel;
	
	return scsa_random_values;
}

int* multi_channel_random_add_random_parameter_generator(int patch_size_rows, int patch_size_cols){
	int mcra_first_random_row = get_random_number_in_range(0, patch_size_rows - 1);
	int mcra_second_random_row = get_random_number_in_range(0, patch_size_rows - 1);
	int mcra_first_random_col = get_random_number_in_range(0, patch_size_cols - 1);
	int mcra_second_random_col = get_random_number_in_range(0, patch_size_cols - 1);
	int mcra_channel_one = get_random_number_in_range(0,2);
	int mcra_channel_two = get_random_number_in_range(0,2);
		
	int* random_values = new int[6];
	random_values[0] = mcra_first_random_row;
	random_values[1] = mcra_first_random_col;
	random_values[2] = mcra_second_random_row;
	random_values[3] = mcra_second_random_col;
	random_values[4] = mcra_channel_one;
	random_values[5] = mcra_channel_two;
	
	return random_values;
}
int* single_channel_add_random_parameter_generator(int patch_size_rows, int patch_size_cols){
	int first_random_row = get_random_number_in_range(0, patch_size_rows - 1);
	cout << "random first row " << first_random_row << endl;
	int second_random_row = get_random_number_in_range(0, patch_size_rows - 1);
	int first_random_col = get_random_number_in_range(0, patch_size_cols - 1);
	int second_random_col = get_random_number_in_range(0, patch_size_cols - 1);
	int channel = get_random_number_in_range(0,2);
	int* sca_random_values = new int[5];
		
	sca_random_values[0] =first_random_row;
	sca_random_values[1] = first_random_col;
	sca_random_values[2] = second_random_row;
	sca_random_values[3] = second_random_col;
	sca_random_values[4] = channel;
	return sca_random_values;
}



typedef SemanticTextonForest::region_prior_integral_image region_prior_integral_image;

SemanticTextonForest::SemanticTextonForest(unordered_map<RGB, int> colors_for_labels, int number_of_classes, int patch_size_rows, int patch_size_cols, int number_of_trees, int sample_frequency, int img_rows, int img_cols) {
	this->colors_for_labels = colors_for_labels;
	this->patch_size_rows = patch_size_rows;
	this->patch_size_cols = patch_size_cols;
	this->number_of_trees = number_of_trees;
	this->sample_frequency = sample_frequency;
	this->img_rows = img_rows;
	this->img_cols = img_cols;
	for(unordered_map<RGB, int>::iterator it = colors_for_labels.begin(); it != colors_for_labels.end(); it++){
			this->label_to_color_map.insert(make_pair(it->second,it->first));
	}
	
	//number of classes means class labels are [0, number_of_classes-1]
	this->number_of_classes = number_of_classes;
	this->max_class_index = this->number_of_classes - 1;
	this->soft_label_functors = create_functors();
	this->region_prior_functors = create_region_prior_functors();
}

//unordered_map<int, STFRandomTreeFunctor*> SemanticTextonForest::create_functor_look_up_table(){
vector<SoftLabelRandomTreeFunctor> SemanticTextonForest::create_functors(){

	//number of functions should be multiple of 5 (as we have 5 functions)
	int number_of_functions = 500;
	vector<SoftLabelRandomTreeFunctor> functors;
	
	// insert blank placeholder for each function below so we can then use the lut[id] = syntax
	functors.resize(number_of_functions);
	
	int functor_hash_key = 0; 
	for(int i = 0; i < number_of_functions/5; i++){
		
		SoftLabelRandomTreeFunctor single_channel_add_functor = SoftLabelRandomTreeFunctor(0, single_channel_add, functor_hash_key, single_channel_subtract_abs_random_parameter_generator(patch_size_rows, patch_size_cols));
		functors[functor_hash_key] = single_channel_add_functor;
		functor_hash_key++;
		SoftLabelRandomTreeFunctor single_channel_subtract_abs_functor = SoftLabelRandomTreeFunctor(0, single_channel_subtract_abs, functor_hash_key, single_channel_subtract_abs_random_parameter_generator(patch_size_rows, patch_size_cols));
		functors[functor_hash_key] =  single_channel_subtract_abs_functor;
		functor_hash_key++;		
		SoftLabelRandomTreeFunctor multi_channel_random_add_functor = SoftLabelRandomTreeFunctor(0, multi_channel_random_add, functor_hash_key, multi_channel_random_add_random_parameter_generator(patch_size_rows, patch_size_cols));
		functors[functor_hash_key] = multi_channel_random_add_functor;
		functor_hash_key++;
		SoftLabelRandomTreeFunctor multi_channel_random_subtract_abs_functor = SoftLabelRandomTreeFunctor(0, multi_channel_random_subtract_abs, functor_hash_key, multi_channel_random_add_random_parameter_generator(patch_size_rows, patch_size_cols));
		functors[functor_hash_key] = multi_channel_random_subtract_abs_functor;
		functor_hash_key++;
		SoftLabelRandomTreeFunctor value_at_random_pixel_and_channel_functor = SoftLabelRandomTreeFunctor(0, value_at_random_pixel_and_channel, functor_hash_key, value_at_random_pixel_random_parameter_generator(patch_size_rows, patch_size_cols));
		functors[functor_hash_key] = value_at_random_pixel_and_channel_functor;
		functor_hash_key++;
	}
		return functors;
}


void SemanticTextonForest::update_soft_label_functors_cache(vector<Mat>* cache){
	for(vector<SoftLabelRandomTreeFunctor>::iterator it = soft_label_functors.begin(); it != soft_label_functors.end(); it++){
		it->set_image_cache(cache);
	}
}

void SemanticTextonForest::update_region_prior_functors_cache(std::vector<region_prior_integral_image*>* cache){
	for(vector<RegionPriorTreeFunctor>::iterator it = region_prior_functors.begin(); it != region_prior_functors.end(); it++){
			it->set_image_cache(cache);
	}
}


vector<RegionPriorTreeFunctor> SemanticTextonForest::create_region_prior_functors(){
	int number_of_functors = 500;
	
	
	int max_rect_rows = 100;
	int min_rect_rows = 1;
	
	int max_rect_cols = 100;
	int min_rect_cols = 1;

	int max_offset_rows = (img_rows/4) - max_rect_rows;
	int min_offset_rows = -1*max_offset_rows;
		
	int max_offset_cols = (img_cols/4) - max_rect_cols;
	int min_offset_cols = -1*max_offset_cols;
		
	
	vector<RegionPriorTreeFunctor> functors;
	functors.resize(number_of_functors);
	
	for(int i = 0; i < number_of_functors; i++){
		RegionPriorTreeFunctor rptf = RegionPriorTreeFunctor(0, i, img_rows, img_cols, 
				get_random_number_in_range(min_offset_rows, max_offset_rows), 
				get_random_number_in_range(min_offset_cols, max_offset_cols),
				get_random_number_in_range(min_rect_rows, max_rect_rows),
				get_random_number_in_range(min_rect_cols, max_rect_cols),
				get_random_number_in_range(0, max_class_index));
		
		functors[i] =  rptf;
	}
	
	return functors;
}



unordered_map<int, double> SemanticTextonForest::get_class_label_weights(vector<TrainingDataPoint>& data_points){
	
	unordered_map<int, double> label_weights;
	
	for(vector<TrainingDataPoint>::iterator it = data_points.begin(); it < data_points.end(); it++){
		label_weights[it->get_class_label()]++;
	}
	vector<int> class_labels;
	
	for(unordered_map<int, double>::iterator it = label_weights.begin(); it != label_weights.end(); it++){
		class_labels.push_back(it->first);
	}
	
	for(vector<int>::iterator it = class_labels.begin(); it < class_labels.end(); it++){
		label_weights[*it] = 1.0/label_weights[*it]; 
	}
	return label_weights;
	
}

void SemanticTextonForest::add_class_weight_to_each_point(unordered_map<int, double>& class_label_weights, vector<TrainingDataPoint>& all_data_points){
	for(vector<TrainingDataPoint>::iterator it = all_data_points.begin(); it != all_data_points.end(); it++){
		it->set_class_weight(class_label_weights.at(it->get_class_label()));
	}
}


void SemanticTextonForest::train(vector<path>& training_image_files, string& ground_truth_suffix){
	
	TrainingDataPointGenerator data_point_generator = TrainingDataPointGenerator();
	vector<TrainingDataPoint> all_data_points;
	unordered_map<string, int> filename_to_hash_key_map;
	int hash_key = 0;
	for(vector<path>::iterator file = training_image_files.begin(); file != training_image_files.end(); file++){
		if(filename_to_hash_key_map.find(file->string()) == filename_to_hash_key_map.end()){
			filename_to_hash_key_map.insert(make_pair(file->string(), hash_key));
			hash_key++;
		}
	}
	
	all_data_points = data_point_generator.get_training_data_points(training_image_files, filename_to_hash_key_map, ground_truth_suffix, colors_for_labels, sample_frequency, patch_size_rows, patch_size_cols);
	
	//split the data into 4 sets
	vector<vector<TrainingDataPoint> > data_splits = split_training_data_points(all_data_points, number_of_trees);
	
	vector<Mat>* preloaded_images = load_images(all_data_points);
	
	update_soft_label_functors_cache(preloaded_images);
	
	soft_label_random_forest = vector<RandomTree<TrainingDataPoint, SoftLabelRandomTreeFunctor> >();
	
	for(int i = 0; i < number_of_trees; i++){
		cout << "****************building tree " << i+1 << " of " << number_of_trees << "********************" << endl;
		vector<TrainingDataPoint> data_point_split = data_splits.at(i);
		unordered_map<int, double> class_label_weights = get_class_label_weights(data_point_split);
		add_class_weight_to_each_point(class_label_weights, data_point_split);
		
		RandomTree<TrainingDataPoint, SoftLabelRandomTreeFunctor> tree = RandomTree<TrainingDataPoint, SoftLabelRandomTreeFunctor>(data_point_split, soft_label_functors, class_label_weights, number_of_classes, 5, 5, 9);
		tree.build();
		soft_label_random_forest.push_back(tree);
	}

	delete preloaded_images;
	
	//the last iteration of the above loop adds 1 to it, so we reduce it here
	int max_hash_key = hash_key - 1;
	vector<region_prior_integral_image*>* images = create_region_prior_integral_images(filename_to_hash_key_map, max_hash_key);
	
	update_region_prior_functors_cache(images);
	
	segmentation_random_forest = vector<RandomTree<TrainingDataPoint, RegionPriorTreeFunctor> >();
		
	for(int i = 0; i < number_of_trees; i++){
		cout << "****************building segmentation tree " << i+1 << " of " << number_of_trees << "********************" << endl;
		vector<TrainingDataPoint> data_point_split = data_splits.at(i);
		unordered_map<int, double> class_label_weights = get_class_label_weights(data_point_split);
		add_class_weight_to_each_point(class_label_weights, data_point_split);
		RandomTree<TrainingDataPoint, RegionPriorTreeFunctor> tree = RandomTree<TrainingDataPoint, RegionPriorTreeFunctor>(data_point_split, region_prior_functors, class_label_weights, number_of_classes, 5, 5, 9);
		tree.build();
		segmentation_random_forest.push_back(tree);
	}

	//delete images
	for(vector<region_prior_integral_image*>::iterator it = images->begin(); it != images->end(); it++){
		delete (*it);
	}
	delete images;
}

vector<region_prior_integral_image*>* SemanticTextonForest::create_region_prior_integral_images(unordered_map<string, int>& filename_to_hash_key_map, int max_hash_key_value){
	
	const int rows_extension = (patch_size_rows - 1);
	const int cols_extension = (patch_size_cols - 1);
	
	vector<region_prior_integral_image*>* images = new vector<region_prior_integral_image*>();
	images->resize(max_hash_key_value + 1);
	
	vector<Mat> preloaded_extended_image;
	const int extended_image_hash_key = 0;
	
	for(unordered_map<string, int>::iterator it = filename_to_hash_key_map.begin(); it != filename_to_hash_key_map.end(); it++){
		Mat original = imread(it->first, CV_LOAD_IMAGE_COLOR);
		Mat extended_image = original;
		//extend image around the border so we can extract patches around the borer pixels for testing
		copyMakeBorder(original, extended_image, rows_extension/2, rows_extension/2, cols_extension/2, cols_extension/2, BORDER_REPLICATE);
		preloaded_extended_image.insert(preloaded_extended_image.begin() + extended_image_hash_key, extended_image);
		update_soft_label_functors_cache(&preloaded_extended_image);
		(*images)[it->second] = create_region_prior_integral_image(extended_image,extended_image_hash_key,rows_extension, cols_extension);
	}
	
	return images;
}

template<class DataPoint>
vector<Mat>* SemanticTextonForest::load_images(vector<DataPoint>& data){
	
	unordered_map<int, Mat> loaded_images;
	int max_image_hash_key = 0;
	for(typename vector<DataPoint>::iterator it = data.begin(); it != data.end(); it++){
		if(loaded_images.find(it->get_image_hash_key()) == loaded_images.end()){
			loaded_images[it->get_image_hash_key()] = imread(it->get_image_filename());
			if(it->get_image_hash_key() > max_image_hash_key){
				max_image_hash_key = it->get_image_hash_key();
			}
		}
	}

	
	vector<Mat>* images = new vector<Mat>();
	images->resize(max_image_hash_key + 1);
	//need the +1 so we can do image_arr[hash_key] (hash key starts from 0) = 
	
	for(unordered_map<int, Mat>::iterator it = loaded_images.begin(); it != loaded_images.end(); it++){
		(*images)[it->first] = it->second;
	}
	
	return images;
}


region_prior_integral_image* SemanticTextonForest::create_region_prior_integral_image(Mat& extended_image, const int image_hash_key,
		const int rows_extension, const int cols_extension){	
	//the +1's are for the integral image
	region_prior_integral_image* rp_integral_image = new region_prior_integral_image(boost::extents[img_rows + 1][img_cols + 1][number_of_classes]);
	region_prior_integral_image& rpii_ref = *rp_integral_image;
	for(int i = 0; i < 1; i++){
		for(int j = 0; j < 1; j++){
			for(int k = 0; k < number_of_classes; k++){
				rpii_ref[i][j][k] = 0.0;
			}
		}
	}
	
	double joint_hist[number_of_classes];
	
	int row_extension_each_side = rows_extension/2;
	int col_extension_each_side = cols_extension/2;
	
	for(int ext_row = row_extension_each_side; ext_row < extended_image.rows - row_extension_each_side; ext_row++){
		for(int ext_col = col_extension_each_side; ext_col < extended_image.cols - col_extension_each_side; ext_col++ ){
			int img_row = ext_row - row_extension_each_side;
			int img_col = ext_col - col_extension_each_side;

			populate_joint_leaf_node_histogram(soft_label_random_forest, soft_label_functors, joint_hist,img_row, img_col, image_hash_key);
			//II(x,y_ = image(x,y) + II(x-1, y) + II (x, y-1) - II(x-1, y-1)
			for(int class_label = 0; class_label < number_of_classes; class_label++){
				rpii_ref[img_row + 1][img_col + 1][class_label] = joint_hist[class_label] 
				                                                 + rpii_ref[img_row][img_col + 1][class_label] 
				                                                 + rpii_ref[img_row + 1][img_col][class_label]
				                                                 - rpii_ref[img_row][img_col][class_label];			
			}
		}
	}
	
	return rp_integral_image;
}

Mat SemanticTextonForest::segment_image_2(path& image_file){
	boost::timer::auto_cpu_timer t;
		
	const int rows_extension = (patch_size_rows - 1);
	const int cols_extension = (patch_size_cols - 1);
		
	string filename = image_file.string();
		
	Mat original = imread(filename, CV_LOAD_IMAGE_COLOR);
	Mat extended_image = original;
	string extended_image_filename = "extended_image";

	//extend image around the border so we can extract patches around the borer pixels for testing
	copyMakeBorder(original, extended_image, rows_extension/2, rows_extension/2, cols_extension/2, cols_extension/2, BORDER_REPLICATE);
	Mat segmented_img = Mat(original.rows, original.cols, original.type());

	//create cache with filename of orginal pointing to the extended image 
	int image_hash_key = 0;
	vector<Mat> preloaded_images;
	preloaded_images.insert(preloaded_images.begin() + image_hash_key, extended_image);

	//re-initialise functors with new cache 
	update_soft_label_functors_cache(&preloaded_images);
	
	int integral_image_hash_key = 0;
	vector<region_prior_integral_image*>*  region_prior_images = new vector<region_prior_integral_image*>();
	
	region_prior_images->push_back(create_region_prior_integral_image(extended_image, image_hash_key, rows_extension, cols_extension));
	update_region_prior_functors_cache(region_prior_images);
	
//	for(int i = 0; i < segmented_img.rows; i++){
//		for(int j = 0; j < segmented_img.cols; j++){
//			int max_class_label = calculate_most_likely_class(segmentation_random_forest, region_prior_functors, i, j, integral_image_hash_key);
//			RGB rgb = label_to_color_map.at(max_class_label);
//								
//			Point3_<uchar>* pixel = segmented_img.ptr<Point3_<uchar> >(i, j);
//			pixel->z = rgb.red;
//			pixel->y = rgb.green;
//			pixel->x = rgb.blue;
//		}
//	}
	return segmented_img;
}


Mat SemanticTextonForest::segment_image(path& image_file){
	boost::timer::auto_cpu_timer t;
	
	const int rows_extension = (patch_size_rows - 1);
	const int cols_extension = (patch_size_cols - 1);
	
	string filename = image_file.string();
	
	Mat original = imread(filename, CV_LOAD_IMAGE_COLOR);
	Mat extended_image = original;
	string extended_image_filename = "extended_image";
	
	//extend image around the border so we can extract patches around the borer pixels for testing
	copyMakeBorder(original, extended_image, rows_extension/2, rows_extension/2, cols_extension/2, cols_extension/2, BORDER_REPLICATE);
	Mat segmented_img = Mat(original.rows, original.cols, original.type());

	//create cache with filename of orginal pointing to the extended image 
	int image_hash_key = 0;
	vector<Mat> preloaded_images;
	preloaded_images.insert(preloaded_images.begin() + image_hash_key, extended_image);
	
	//re-initialise functors with new cache 
	update_soft_label_functors_cache(&preloaded_images);
	
	for(int ext_row = rows_extension; ext_row < extended_image.rows - rows_extension; ext_row++){
		for(int ext_col = cols_extension; ext_col < extended_image.cols - cols_extension; ext_col++ ){
			int img_row = ext_row - rows_extension;
			int img_col = ext_col - cols_extension;

			int max_class_label = calculate_most_likely_class(soft_label_random_forest, soft_label_functors, ext_row - rows_extension, ext_col - cols_extension, image_hash_key);

			RGB rgb = label_to_color_map.at(max_class_label);
					
			Point3_<uchar>* pixel = segmented_img.ptr<Point3_<uchar> >(img_row, img_col);
			pixel->z = rgb.red;
			pixel->y = rgb.green;
			pixel->x = rgb.blue;
			
		}
	}
	return segmented_img;
}

template<class T>
int SemanticTextonForest::calculate_most_likely_class(
		vector<RandomTree<TrainingDataPoint, T> >& forest,
		vector<T>& functors,
		int param1, int param2, int image_hash_key){
	double joint_hist[number_of_classes];
	populate_joint_leaf_node_histogram(forest, functors, joint_hist, param1, param2, image_hash_key);
	
	double max = 0;
	int max_class = 0;;
	for(int i = 0; i < number_of_classes; i++){
		if(joint_hist[i] >= max){
			max_class = i;
			max = joint_hist[i];
		}
	}
	
	return max_class;
}


template<class T>
void SemanticTextonForest::populate_joint_leaf_node_histogram(
		vector<RandomTree<TrainingDataPoint, T> >& forest,
		vector<T>& functors,
		double joint_hist[], const int& param1, const int& param2, const int& image_hash_key){
	fill_n(joint_hist, number_of_classes, 0);
			
	for(typename vector<RandomTree<TrainingDataPoint, T> >::iterator it = forest.begin(); it != forest.end(); it++){
		double* leaf_node_dist = get_normalised_leaf_node_histogram(functors, it->get_root(), param1, param2, image_hash_key, 0);	
		for(int i = 0; i < number_of_classes; i++){
			joint_hist[i] += leaf_node_dist[i];
		}
	}
	//do we need to case number_of_trees to double?
	for(int i = 0; i < number_of_classes; i++){
				joint_hist[i] /= number_of_trees;
	}
}

template<class T>
double* SemanticTextonForest::get_normalised_leaf_node_histogram(
		vector<T>& functors,
		RandomTreeNode<TrainingDataPoint, T>& current, 
		int param1,
		int param2,
		int image_hash_key,
		int depth)
{
	if(current.is_leaf()){
		return current.get_class_distribution();
	}
		
	double threshold = current.get_threshold();
	double value = (functors.at(current.get_functor_id()))(param1, param2, image_hash_key);
		
	if(value <= threshold){
		return get_normalised_leaf_node_histogram(functors, current.get_left_child(), param1, param2, image_hash_key, depth + 1); 
	}
	else{
		return get_normalised_leaf_node_histogram(functors, current.get_right_child(), param1, param2, image_hash_key, depth + 1);
	}
}


//takes in a copy of training_data_points (note no &) so the get_and_remove will only effect local copy
vector<vector<TrainingDataPoint> > SemanticTextonForest::split_training_data_points(vector<TrainingDataPoint> training_data_points, int number_of_splits){
	vector<vector<TrainingDataPoint> > splits;

	int points_to_remove_per_split = training_data_points.size() * 0.25;

	for(int i = 0; i < number_of_splits; i++){
		vector<TrainingDataPoint> split = vector<TrainingDataPoint>(training_data_points);
		for(int j = 0; j < points_to_remove_per_split; j++){
			get_and_remove_random_item(split);
		}
		splits.push_back(split);
	}

	return splits;
}

//random functions
inline double single_channel_add(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_key,
		vector<Mat>* preloaded_images,
		int* random_values){
	
	int first_pixel_row = patch_top_left_row + random_values[0];
	int first_pixel_col = patch_top_left_col + random_values[1];
	int second_pixel_row = patch_top_left_row + random_values[2];
	int second_pixel_col = patch_top_left_col + random_values[3];
	
	int channel = random_values[4];
	
	Vec3b* first_pixel_bgr = (*preloaded_images)[image_hash_key].ptr<Vec3b>(first_pixel_row, first_pixel_col);
	Vec3b* second_pixel_bgr = (*preloaded_images)[image_hash_key].ptr<Vec3b>(second_pixel_row, second_pixel_col);
	
	return (*first_pixel_bgr)[channel] + (*second_pixel_bgr)[channel];
}


inline double multi_channel_random_add(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_key,
		vector<Mat>* preloaded_images,
		int* random_values){
		
	
	int first_pixel_row = patch_top_left_row + random_values[0];
	int first_pixel_col = patch_top_left_col + random_values[1];
	int second_pixel_row =patch_top_left_row + random_values[2];
	int second_pixel_col = patch_top_left_col + random_values[3];
		
	
	int first_pixel_channel = random_values[4];
	int second_pixel_channel = random_values[5];
	
	return (*(*preloaded_images)[image_hash_key].ptr<Vec3b>(first_pixel_row, first_pixel_col))[first_pixel_channel] + 
			(*(*preloaded_images)[image_hash_key].ptr<Vec3b>(second_pixel_row, second_pixel_col))[second_pixel_channel];
	
}

inline double single_channel_subtract_abs(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_key,
		vector<Mat>* preloaded_images,
		int* random_values){
			
	int first_pixel_row = patch_top_left_row + random_values[0];
	int first_pixel_col = patch_top_left_col + random_values[1];
	int second_pixel_row =patch_top_left_row + random_values[2];
	int second_pixel_col = patch_top_left_col + random_values[3];
		
	
	int channel = random_values[4];
		
	Vec3b* first_pixel_bgr = (*preloaded_images)[image_hash_key].ptr<Vec3b>(first_pixel_row, first_pixel_col);
	Vec3b* second_pixel_bgr = (*preloaded_images)[image_hash_key].ptr<Vec3b>(second_pixel_row, second_pixel_col);
	
	int value = (*first_pixel_bgr)[channel] + (*second_pixel_bgr)[channel]; 
	return abs(value);	
}


inline double multi_channel_random_subtract_abs(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_key,
		vector<Mat>* preloaded_images,
		int* random_values){
	
	int first_pixel_row = patch_top_left_row + random_values[0];
	int first_pixel_col = patch_top_left_col + random_values[1];
	int second_pixel_row =patch_top_left_row + random_values[2];
	int second_pixel_col = patch_top_left_col + random_values[3];
	
	int first_pixel_channel = random_values[4];
	int second_pixel_channel = random_values[5];

	
	Vec3b *first = (*preloaded_images)[image_hash_key].ptr<Vec3b>(first_pixel_row, first_pixel_col);
	Vec3b *second = (*preloaded_images)[image_hash_key].ptr<Vec3b>(second_pixel_row, second_pixel_col); 
	
	int value = (*first)[first_pixel_channel]-(*second)[second_pixel_channel];
	return abs(value);
	
}

inline double value_at_random_pixel_and_channel(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_key,
		vector<Mat>* preloaded_images,
		int* random_values){
		
	int pixel_row =patch_top_left_row + random_values[0];
	int pixel_col = patch_top_left_col + random_values[1];
	
	int channel = random_values[2];

	Vec3b* pixel = (*preloaded_images)[image_hash_key].ptr<Vec3b>(pixel_row, pixel_col);
	
	return (*pixel)[channel];
}

