/*
 * Node.h
 *
 *  Created on: 22 Oct 2012
 *      Author: Aly
 */

#ifndef NODE_H_
#define NODE_H_

template<class T>
class Node{
private:
	Node<T> *child;
public:
	Node(): child(0){}
	void create_child(){
		child = &Node<T>();
	}
};

#endif /* NODE_H_ */
