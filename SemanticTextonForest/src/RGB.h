/*
 * RGB.h
 *
 *  Created on: 10 Oct 2012
 *      Author: Aly
 */

#ifndef RGB_H_
#define RGB_H_


struct RGB {
	int red;
	int green;
	int blue;

	RGB(int red, int green, int blue) {
		this->red = red;
		this->green = green;
		this->blue = blue;
	}

};

inline std::size_t hash_value(RGB const &rgb)
{
	size_t seed = 0;
	boost::hash_combine(seed, rgb.red);
	boost::hash_combine(seed, rgb.green);
	boost::hash_combine(seed, rgb.blue);
	return seed;
}

inline bool operator==(RGB const &left, RGB const &right) {
	return left.red == right.red && left.green == right.green && left.blue == right.blue;
}

#endif /* RGB_H_ */
