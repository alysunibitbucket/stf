/*
 * SemanticTextonForest.h
 *
 *  Created on: 12 Oct 2012
 *      Author: Aly
 */

#ifndef SEMANTICTEXTONFOREST_H_
#define SEMANTICTEXTONFOREST_H_

#include <boost/filesystem.hpp>
#include <vector>
#include <string>
#include <boost/unordered_map.hpp>
#include "RGB.h"
#include "TrainingDataPoint.h"
#include "RandomTree.h"
#include <opencv2/opencv.hpp>
#include "boost/multi_array.hpp"
#include "STFRandomTreeFunction.h"

class SemanticTextonForest {
public:
	typedef RegionPriorTreeFunctor::region_prior_integral_image region_prior_integral_image;
private:
	std::vector<std::vector<TrainingDataPoint> >split_training_data_points(std::vector<TrainingDataPoint> training_data_points, int number_of_splits);
	
	std::vector<RandomTree<TrainingDataPoint, SoftLabelRandomTreeFunctor> > soft_label_random_forest;
	std::vector<RandomTree<TrainingDataPoint, RegionPriorTreeFunctor> > segmentation_random_forest;
	
	boost::unordered_map<RGB, int> colors_for_labels;
	boost::unordered_map<int, RGB> label_to_color_map;
	std::vector<SoftLabelRandomTreeFunctor> soft_label_functors;
	std::vector<RegionPriorTreeFunctor> region_prior_functors;
	int sample_frequency;
	int patch_size_rows;
	int patch_size_cols;
	int number_of_trees;
	int number_of_classes;
	int max_class_index;
	//This implementation assumes testing and training images are all of same resolution
	int img_rows;
	int img_cols;
	template<class DataPoint>
	std::vector<cv::Mat>* load_images(std::vector<DataPoint>& data_points);
	
	std::vector<SoftLabelRandomTreeFunctor> create_functors();
	std::vector<RegionPriorTreeFunctor> create_region_prior_functors();
	
	void update_soft_label_functors_cache(std::vector<cv::Mat>* cache);
	void update_region_prior_functors_cache(std::vector<region_prior_integral_image*>* cache);
	boost::unordered_map<int, double> get_class_label_weights(std::vector<TrainingDataPoint>& training_data_points);
	void add_class_weight_to_each_point(boost::unordered_map<int, double>& class_label_weights, std::vector<TrainingDataPoint>& all_data_points);
	template<class T>
	int calculate_most_likely_class(
			std::vector<RandomTree<TrainingDataPoint, T> >& forest,
			std::vector<T>& functors,
			int param1, int param2, int image_hash_key);
	
	template<class T>
	void populate_joint_leaf_node_histogram(
			std::vector<RandomTree<TrainingDataPoint, T> >& forest,
			std::vector<T>& functors,
			double joint_his[], const int& param1, const int& param2, const int& image_hash_key);
	template<class T>
	double* get_normalised_leaf_node_histogram(
			std::vector<T>& functors,
			RandomTreeNode<TrainingDataPoint, T>& current, 
			int param1, int param2, int image_hash_key, int depth);
	
	std::vector<region_prior_integral_image*>* create_region_prior_integral_images(boost::unordered_map<std::string, int>& filename_to_hash_key_map, int max_hash_key_value);
	
	region_prior_integral_image* create_region_prior_integral_image(cv::Mat& extended_image, const int image_hash_key,
			const int rows_extension, const int cols_extension);
	
public:
	SemanticTextonForest(boost::unordered_map<RGB, int> colors_for_labels, int number_of_classes, int patch_size_rows, int patch_size_cols, int number_of_trees, int sample_frequency, int img_rows, int img_cols);
	void train(std::vector<boost::filesystem::path>& training_image_files, std::string& ground_truth_suffix);
	cv::Mat segment_image(boost::filesystem::path& image_file);
	cv::Mat segment_image_2(boost::filesystem::path& image_file);
	void save_random_forest(std::string filename);
	
	~SemanticTextonForest(){
	}
};

#endif /* SEMANTICTEXTONFOREST_H_ */
