/*
 * DataSplit.h
 *
 *  Created on: 12 Oct 2012
 *      Author: Aly
 */

#ifndef DATASPLIT_H_
#define DATASPLIT_H_

#include <boost/filesystem.hpp>
#include <vector>

class DataSplit {
private:
	std::vector<boost::filesystem::path> test_image_files;
	std::vector<boost::filesystem::path> training_image_files;
	std::vector<boost::filesystem::path> get_image_files(boost::filesystem::path& image_dir, std::string& ground_truth_suffix, std::string& image_format, bool ground_truth_files);
public:
	DataSplit(boost::filesystem::path test_image_files, boost::filesystem::path training_images_path, std::string ground_truth_suffix, std::string image_format);
	std::vector<boost::filesystem::path> get_test_image_files();
	std::vector<boost::filesystem::path> get_training_image_files();
};

#endif /* DATASPLIT_H_ */
