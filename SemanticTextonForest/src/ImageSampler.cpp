/*
 * ImageSampler.cpp
 *
 *  Created on: 12 Oct 2012
 *      Author: Aly
 */

#include <opencv2/opencv.hpp>
#include "ImageSampler.h"
#include <vector>
#include <boost/filesystem.hpp>
#include <boost/unordered_map.hpp>
#include "RGB.h"
#include "utils.hpp"

using namespace std;
using namespace boost;
using namespace boost::filesystem;
using namespace cv;

ImageSampler::ImageSampler() {
}


vector<TrainingDataPoint> ImageSampler::sample_data_points(path& file, path& ground_truth_file, int sample_frequency, 
		int patch_size_rows, int patch_size_cols, 
		unordered_map<RGB, int>& colors_for_labels,
		unordered_map<string, int>& filename_to_hash_key_map)
{
	vector<TrainingDataPoint> data_points;
	//	we minus one as the row and col of the pixel add one row and col to the patch
	int row_trim = (patch_size_rows - 1)/2;
	int col_trim = (patch_size_cols - 1) /2;
	
	Mat img = imread(file.string(), CV_LOAD_IMAGE_COLOR);
	Mat ground_truth_img = imread(ground_truth_file.string(), CV_LOAD_IMAGE_COLOR);
	RGB rgb(0,0,0);
	int i = 0;
	for(int y =  row_trim; y < img.rows - row_trim; y += sample_frequency ){
		for(int x = col_trim; x < img.cols - col_trim; x += sample_frequency){
			Point3_<uchar>* pixel = ground_truth_img.ptr<Point3_<uchar> >(y, x);
			rgb.red = (int) pixel->z;
			rgb.green = (int) pixel->y;
			rgb.blue = (int) pixel->x;
			int ground_truth_label = colors_for_labels[rgb];
			//create image patch of box_size centered around the pixel
			
			// the pixel that the training data point is based on is now located at (row_trim, col_trim) in the img patch
			string filename = file.string();

			if(filename_to_hash_key_map.find(filename) == filename_to_hash_key_map.end()){
				cerr << "filename " << filename << " has no hash key in map, exiting" << endl;
				exit(-1);
			}	
			
			data_points.push_back(TrainingDataPoint(y, x, ground_truth_label, y - row_trim, x - col_trim, patch_size_rows, patch_size_cols, filename_to_hash_key_map[filename], filename));
			i++;
		}
	}
	
	return data_points;
}
