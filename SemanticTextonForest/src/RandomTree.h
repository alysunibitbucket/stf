/*
 * RandomTree.h
 *
 *  Created on: 11 Oct 2012
 *      Author: Aly
 */

#ifndef RANDOMTREE_H_
#define RANDOMTREE_H_

#include "utils.hpp"
#include <cmath>
#include <ctime>
#include <boost/unordered_map.hpp>
#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <opencv2/opencv.hpp>
#include "RandomTreeNode.h"

template <class DataPoint, typename RandomFunctor>
class RandomTree {
private:
	
	RandomTreeNode<DataPoint, RandomFunctor> *root;
	struct node_data_split {
		std::vector<DataPoint> left_data;
		std::vector<DataPoint> right_data;
		RandomFunctor functor;
		double threshold;
		double information_gain;
		
		node_data_split(std::vector<DataPoint>& left, std::vector<DataPoint>& right, RandomFunctor& functor,double threshold, double information_gain)
		:left_data(left), right_data(right), functor(functor), threshold(threshold), information_gain(information_gain) {};
	};
	

	bool data_has_same_values(std::vector<DataPoint>& data){
		DataPoint first = *(data.begin());

		for(typename std::vector<DataPoint>::iterator it = data.begin() + 1; it != data.end(); it++){
			if(*it != first){
				return false;
			}
		}
		
		return true;
	}
	
	/*
	 * returns a pair containing [value, data point] 
	 */
	std::vector<std::pair<double,DataPoint> > compute_value_data_pairs(std::vector<DataPoint>& data, RandomFunctor& functor)
	{
		std::vector<std::pair<double,DataPoint> > value_data_pairs;
		value_data_pairs.reserve(data.size());
		
		for(typename std::vector<DataPoint>::iterator data_point = data.begin(); data_point < data.end(); data_point++){
			double value = functor(*data_point);
			std::pair<double, DataPoint> value_data_pair =  std::pair<double, DataPoint>(value, *data_point);
			value_data_pairs.push_back(value_data_pair);
		}
		
		return value_data_pairs;
	}
	
	
	double calculate_mean(std::vector<std::pair<double, DataPoint> >& value_data_pairs){
		double mean = 0;
		for(typename std::vector<std::pair<double,DataPoint> >::iterator x = value_data_pairs.begin(); x < value_data_pairs.end(); x++){
			mean += x->first;
		}
		
		return mean/((double)value_data_pairs.size());
	}
	
	double calculate_standard_deviation(std::vector<std::pair<double, DataPoint> >& value_data_pairs, double& mean){
		double variance = 0;
		for(typename std::vector<std::pair<double, DataPoint> >::iterator x = value_data_pairs.begin(); x < value_data_pairs.end(); x++){
			variance += (x->first - mean ) * (x->first - mean);
		}
		variance /= (double)(value_data_pairs.size() - 1);
		return sqrt(variance);
	}
	
	boost::random::normal_distribution<double> create_gaussian_from_samples(std::vector<std::pair<double,DataPoint> >& sample_value_weight_pairs){
		double mean = calculate_mean(sample_value_weight_pairs);
		double standard_deviation = calculate_standard_deviation(sample_value_weight_pairs, mean);
		return boost::random::normal_distribution<double>(mean, standard_deviation);
	}
		
	double calculate_entropy(boost::unordered_map<int, double>& class_counts, double total_points){
		double entropy = 0;
		for(boost::unordered_map<int, double>::iterator it = class_counts.begin(); it != class_counts.end(); it++){
			double class_proportion = (((double)it->second)/total_points);
			entropy += (class_proportion * log2(class_proportion));
		}
		
		entropy *= -1;
		
		return entropy;
	}
	
	
	double calculate_information_gain(std::vector<std::pair<double, DataPoint> >& left_value_data_pairs, 
			std::vector<std::pair<double, DataPoint> >& right_value_data_pairs,
			boost::unordered_map<int, double>& class_label_weights){					
		
		boost::unordered_map<int, double> left_class_counts;
		double total_data_points_in_left_dist = 0;
		
		boost::unordered_map<int, double> right_class_counts;
		double total_data_points_in_right_dist = 0;
		
		//#calculate class counts
		for(typename std::vector<std::pair<double, DataPoint> >::iterator it = left_value_data_pairs.begin(); it < left_value_data_pairs.end(); it++){
			left_class_counts[(it->second).get_class_label()]++; 
		}
		
		for(boost::unordered_map<int, double>::iterator it = left_class_counts.begin(); it != left_class_counts.end(); it++ ){
			left_class_counts[it->first] = (double)(((double)it->second)*class_label_weights[it->first]);
			total_data_points_in_left_dist += left_class_counts[it->first];
		}

		for(typename std::vector<std::pair<double, DataPoint> >::iterator it = right_value_data_pairs.begin(); it != right_value_data_pairs.end(); it++){
			right_class_counts[(it->second).get_class_label()]++;
		}
		for(boost::unordered_map<int, double>::iterator it = right_class_counts.begin(); it != right_class_counts.end(); it++){
			right_class_counts[it->first] = (double)(((double)it->second)*class_label_weights[it->first]);
			total_data_points_in_right_dist += right_class_counts[it->first];
		}
		
		//calculate entropies 
		double left_entropy = calculate_entropy(left_class_counts, total_data_points_in_left_dist);
		double right_entropy = calculate_entropy(right_class_counts, total_data_points_in_right_dist);
		
		double total_data_points = total_data_points_in_left_dist + total_data_points_in_right_dist;

		double information_gain= (-1*(total_data_points_in_left_dist/total_data_points)*left_entropy) - 
									((total_data_points_in_right_dist/total_data_points)*right_entropy);
		
		
		return information_gain;
	}
	
	node_data_split find_best_node_data_split(std::vector<DataPoint>& data,
												std::vector<RandomFunctor>& functors,
												boost::unordered_map<int, double>& class_label_weights, 
												int number_of_functions_to_try, int number_of_thresholds_to_try){
		

		typedef boost::mt19937 rng;
		typedef boost::random::normal_distribution<double> gaussian;
		typedef boost::variate_generator<rng&, gaussian > variate_generator;
		rng random_number_generator = rng(time(0));

		RandomFunctor *best_functor  = 0;
		double best_information_gain = -DBL_MAX;
		double best_threshold = 0;
		std::vector<std::pair<double, DataPoint> > best_left_data_split;
		std::vector<std::pair<double, DataPoint> > best_right_data_split;
		
		
		for(int i = 0; i < number_of_functions_to_try; i++){
			
			RandomFunctor functor = get_random_item(functors);
	
			
			std::vector<std::pair<double, DataPoint> > value_data_pairs = compute_value_data_pairs(data, functor);
			
			gaussian distribution = create_gaussian_from_samples(value_data_pairs);

			variate_generator var_gen = variate_generator(random_number_generator, distribution);

			
			for(int j = 0; j < number_of_thresholds_to_try; j++){
				double threshold = var_gen();
				std::vector<std::pair<double, DataPoint> > left_split;
				std::vector<std::pair<double, DataPoint> > right_split;
						
				for(typename std::vector<std::pair<double, DataPoint> >::iterator it = value_data_pairs.begin(); it < value_data_pairs.end(); it++){
					if(it->first <= threshold){
						left_split.push_back(*it);
					}
					else{
						right_split.push_back(*it);
					}
				}
				
				double information_gain = calculate_information_gain(left_split, right_split, class_label_weights);
				
				if(information_gain > best_information_gain){
					best_information_gain = information_gain;
					best_threshold = threshold;
					if(best_functor != 0){
						best_functor = 0;
					}
					best_functor = new RandomFunctor(functor);
					best_left_data_split = left_split;
					best_right_data_split = right_split;
				}
			}
		}
		
	
		//instead of compute this every time we find a better split, wait untill we have found the best and then compute it
		std::vector<DataPoint> left_data;
		std::vector<DataPoint> right_data;
		
		for(typename std::vector<std::pair<double, DataPoint> >::iterator it = best_left_data_split.begin(); it < best_left_data_split.end(); it++){
			left_data.push_back(it->second);
		}
		for(typename std::vector<std::pair<double, DataPoint> >::iterator it = best_right_data_split.begin(); it < best_right_data_split.end(); it++){
			right_data.push_back(it->second);
		}
		
		 node_data_split best_node_data_split = node_data_split(left_data, right_data, *best_functor, best_threshold, best_information_gain);
		 if(best_functor != 0){
			 best_functor = 0;
		 }
		 return best_node_data_split;
	}
	
	void build_tree(RandomTreeNode<DataPoint, RandomFunctor>& current_node, std::vector<DataPoint>& data, 
			std::vector<RandomFunctor>& functions,
			boost::unordered_map<int, double>& class_label_weights,
			int number_of_functions_to_try, int number_of_thresholds_to_try, int max_depth, int current_depth){
	
		
		if(current_depth == max_depth|| data_has_same_values(data))
		{
			current_node.mark_as_leaf();
			current_node.set_data(data);
			return;
		}
		
		//this is the bit where we determine the best function and threshold for the node
		node_data_split data_split = find_best_node_data_split(data, functions, class_label_weights, number_of_functions_to_try, number_of_thresholds_to_try);
		current_node.set_functor_id(data_split.functor.get_functor_id());
		current_node.set_threshold(data_split.threshold);

		if(data_split.left_data.size() == 0 || data_split.right_data.size() == 0){
			current_node.mark_as_leaf();
			current_node.set_data(data);
			return;
		}
		
		current_node.create_left_child();
		current_node.create_right_child();
		build_tree(current_node.get_left_child(), data_split.left_data, functions, class_label_weights, number_of_functions_to_try, number_of_thresholds_to_try, max_depth, current_depth + 1);
		build_tree(current_node.get_right_child(), data_split.right_data, functions, class_label_weights, number_of_functions_to_try, number_of_thresholds_to_try, max_depth, current_depth + 1);
		
	}
	

	
public:
	
	std::vector<DataPoint> data;
	std::vector<RandomFunctor> functors;
	boost::unordered_map<int, double> class_label_weights;
	int number_of_functions_to_try;
	int number_of_thresholds_to_try;
	unsigned int max_depth;
	int number_of_classes;
	
	RandomTree(std::vector<DataPoint>& data,
			std::vector<RandomFunctor>& functors, 
			boost::unordered_map<int, double>& class_label_weights,
			int number_of_classes,
			int number_of_functions_to_try, 
			int number_of_thresholds_to_try, 
			unsigned int max_depth = 5)
	:data(data), functors(functors), class_label_weights(class_label_weights), number_of_functions_to_try(number_of_functions_to_try),
	 number_of_thresholds_to_try(number_of_thresholds_to_try), max_depth(max_depth), number_of_classes(number_of_classes)
	{
		root = new RandomTreeNode<DataPoint, RandomFunctor>(number_of_classes);
	}
	
	RandomTreeNode<DataPoint, RandomFunctor>& get_root(){
		return *root;
	}
	
	void build(){
		time_t t = time(0);
		std::cout << ctime(&t) << "Building tree" << std::endl;
		
		build_tree(*root, data, functors, class_label_weights, number_of_functions_to_try, number_of_thresholds_to_try, max_depth, 0);
		
		t = time(0);
		std::cout << ctime(&t) << "Finished Building tree" << std::endl;
	}
};

#endif /* RANDOMTREE_H_ */
