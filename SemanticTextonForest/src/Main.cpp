/*
 * Main.cpp
 *
 *  Created on: 12 Oct 2012
 *      Author: Aly
 */
#include "Node.h"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include "opencv2/highgui/highgui.hpp"
#include <boost/regex.hpp>
#include <boost/unordered_map.hpp>
#include "boost/filesystem.hpp"
#include "RGB.h"
#include "RandomTreeNode.h"
#include "TrainingDataPoint.h"
#include "utils.hpp"
#include "DataSplit.h"
#include "ImageSampler.h"
#include "SemanticTextonForest.h"


using namespace std;
using namespace cv;
using namespace boost;
using namespace boost::filesystem;

unordered_map<RGB, string> get_ground_truth_values(const string &filename);



int main(int argc, char* argv[]) {

//Mat gt = imread("/home/aly/workspace/SemanticTextonForest/0016E5_08043_L.png", CV_LOAD_IMAGE_COLOR);
//Mat gen = imread("/home/aly/workspace/SemanticTextonForest/region_priors(0843 - newest).png");
//
//double correct = 0;
//double total = (double)gt.rows * (double)gt.cols;
//cout << "rows is " << gen.rows << endl;
//cout << "cols is " << gen.cols << endl;

//for(int i = 0; i < gt.rows; i ++){
//	for(int j = 0; j < gt.cols; j++){
//		Vec3b *gt_pixel = gt.ptr<Vec3b>(i, j);
//		Vec3b *gen_pixel = gen.ptr<Vec3b>(i,j);
//		if((*gt_pixel)[0] == (*gen_pixel)[0] && (*gt_pixel)[1] == (*gen_pixel)[1] && (*gt_pixel)[2] == (*gen_pixel)[2]){
//			correct++;
//		}
//	}
//}
//
//cout << " overall segmentation percent is " << correct/total << endl;

	
	
//	args should be program.exe label_file image_dir ground_truth_suffix image_format percentage_of_images_per_label_to_train
	if (argc < 5) {
		cout << "please specify following arguments: label_file train_image_dir test_image_dir ground_truth_suffix image_format" << endl;
		return -1;
	}

	string label_file_path = argv[1];
	path train_image_dir = path(argv[2]);
	path test_image_dir = path(argv[3]);
	
	if (!exists(train_image_dir)) {
		cerr << train_image_dir << " does not exist, exiting" << endl;
		exit(-1);
	}
	if(!exists(test_image_dir)){
		cerr << test_image_dir << " does not exist, exiting" << endl;
		exit(-1);
	}
	
	string ground_truth_suffix = argv[4];
	string image_format = argv[5];

	unordered_map<RGB, string> colors_for_labels = get_ground_truth_values(label_file_path);
	int class_label = 0;
	unordered_map<RGB, int> colors_for_labels_int;
	for(unordered_map<RGB, string>::iterator it = colors_for_labels.begin(); it != colors_for_labels.end(); it++){
		colors_for_labels_int.insert(make_pair(it->first, class_label));
		class_label++;
		cout << "Setting class " << it->second << " to value " << colors_for_labels_int[it->first]; 
	}
	
	DataSplit data_split = DataSplit(train_image_dir, test_image_dir, ground_truth_suffix, image_format);
 
	vector<path> training_image_files = data_split.get_training_image_files();
	vector<path> test_image_files = data_split.get_test_image_files();
	
	cout << training_image_files.size() << " training images " << endl;
	for(vector<path>::iterator it = training_image_files.begin(); it < training_image_files.end(); it++){
		cout << it->filename();
	}
	cout << endl;
	cout << test_image_files.size() << " test images " << endl;
	for(vector<path>::iterator it = test_image_files.begin(); it < test_image_files.end(); it++){
		cout << it->filename();
	}
	cout << endl;

	
	SemanticTextonForest stf = SemanticTextonForest(colors_for_labels_int, class_label, 21, 21, 5, 5,720,960);
	cout << "Preparing to train" << endl;
	stf.train(training_image_files, ground_truth_suffix);
	Mat img = stf.segment_image_2(test_image_files.at(0));
	//	Mat img = stf.segment_image(test_image_files.at(0));
	
	namedWindow("window", CV_WINDOW_AUTOSIZE);
	imshow("window", img);

	char k;
	k = waitKey(0);
	while(k != 'ESC'){
		k = waitKey(0);
	}
//
	return 0;
}




unordered_map<RGB, string> get_ground_truth_values(const string &filename) {

	unordered_map<RGB, string> ground_truth_values;

	ifstream label_file(filename.c_str());

	if (!label_file) {
		cout << "Could not read " << filename << " label file, exiting" << endl;
		exit(-1);
	}
	//should be of pattern (red green blue object_name) 
	regex expression("([[:digit:]]+)[[:space:]]([[:digit:]]+)[[:space:]]([[:digit:]]+)[[:space:]]([[:word:]]+)");

	while (!label_file.eof()) {
		string line;
		getline(label_file, line);

		smatch matches;
		if (regex_match(line, matches, expression) && sizeof(matches) >= 5) {
			int red, green, blue;
			istringstream(matches[1]) >> red;
			istringstream(matches[2]) >> green;
			istringstream(matches[3]) >> blue;

			RGB rgb(red, green, blue);

			ground_truth_values[rgb] = matches[4];

		} else {
			cerr << line << " does not match input regex" << endl;
		}
	}

	return ground_truth_values;
}



