/*
 * STFDataPoint.h
 *
 *  Created on: 26 Oct 2012
 *      Author: aly
 */

#ifndef STFDATAPOINT_H_
#define STFDATAPOINT_H_
#include "ImagePoint.h"

class STFDataPoint {
public:

	virtual int get_patch_top_left_row() const = 0;
	virtual int get_patch_top_left_col() const = 0;
	virtual int get_patch_size_rows() const = 0;
	virtual int get_patch_size_cols() const = 0;
	virtual int get_image_hash_key() const = 0;
	virtual int get_row() const = 0;
	virtual int get_col() const = 0;
	virtual std::string get_image_filename() const = 0;
	virtual ~STFDataPoint() = 0;
};
inline STFDataPoint::~STFDataPoint() {}
#endif /* STFDATAPOINT_H_ */
