/*
 * ImagePoint.h
 *
 *  Created on: 22 Oct 2012
 *      Author: Aly
 */

#ifndef IMAGEPOINT_H_
#define IMAGEPOINT_H_

class ImagePoint {
private:
	int row;
	int col;
	
public:
	ImagePoint(int row, int col){
		this->row = row;
		this->col = col;
	}
	
	int get_row() const {
		return this->row;
	}
	
	int get_col() const {
		return this->col;
	}
	
	void set_row(int row){
		this->row = row;
	}
	
	void set_col(int col){
		this->col = col;
	}
};

#endif /* IMAGEPOINT_H_ */
