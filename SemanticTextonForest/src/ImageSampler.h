/*
 * ImageSampler.h
 *
 *  Created on: 12 Oct 2012
 *      Author: Aly
 */

#ifndef IMAGESAMPLER_H_
#define IMAGESAMPLER_H_

#include <boost/filesystem.hpp>
#include <boost/unordered_map.hpp>
#include "RGB.h"
#include <vector>
#include "TrainingDataPoint.h"

class ImageSampler {

public:
	ImageSampler();
	std::vector<TrainingDataPoint> sample_data_points(boost::filesystem::path& file, boost::filesystem::path& ground_truth_file, int sample_frequency, int patch_size_rows, int patch_size_cols, boost::unordered_map<RGB, int>& colors_for_labels, boost::unordered_map<std::string, int>& filename_to_hash_key_map);
};

#endif /* IMAGESAMPLER_H_ */
