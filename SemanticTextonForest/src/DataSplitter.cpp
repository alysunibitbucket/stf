/*
 * DataOrganizer.cpp
 *
 *  Created on: 12 Oct 2012
 *      Author: Aly
 */

#include <opencv2/opencv.hpp>
#include "DataSplitter.h"
#include <boost/filesystem.hpp>
#include <boost/unordered_map.hpp>
#include <vector>
#include "DataSplit.h"
#include "utils.hpp"

using namespace std;
using namespace boost;
using namespace boost::filesystem;
using namespace cv;

DataSplitter::DataSplitter() {	
}

DataSplit DataSplitter::split_data_into_training_and_test(path& image_dir, string& ground_truth_suffix, string& image_format, double& percentage_of_images_to_train_per_label, unordered_map<RGB, string>& colors_for_labels) {
	unordered_map<string, vector<path> > label_to_files_map = this->get_files_per_label(image_dir, ground_truth_suffix, image_format, colors_for_labels);

	vector < path > training_images;
	vector < path > test_images;

	//gets the number of images we require per label for training
	unordered_map<string, int> image_count_per_label;

	for (unordered_map<string, vector<path> >::iterator it = label_to_files_map.begin(); it != label_to_files_map.end(); it++) {
		const string label = it->first;
		const vector<path>* files = &it->second;
		image_count_per_label[label] = (((double) files->size()) * percentage_of_images_to_train_per_label) + 0.5f;
	}

	for (unordered_map<string, vector<path> >::iterator it = label_to_files_map.begin(); it != label_to_files_map.end(); it++) {
		const string label = it->first;
		vector < path > *files = &label_to_files_map[label];
		while (image_count_per_label[label] > 0) {
			//path file = get_and_remove_random_item(*files);
			path file = get_and_remove_random_item(*files);
			training_images.push_back(file);
			image_count_per_label[label]--;

			//remove for all other labels and decrease their count
			for (unordered_map<string, vector<path> >::iterator label_it = label_to_files_map.begin(); label_it != label_to_files_map.end(); label_it++) {
				vector < path > *label_paths = &label_to_files_map[label_it->first];
				vector<path>::iterator pos = find(label_paths->begin(), label_paths->end(), file);
				if (pos != label_paths->end()) {
					label_paths->erase(pos);
					image_count_per_label[label_it->first]--;
				}
			}
		}
	}

//we use a set first otherwise there will be many duplicates
	set < path > test_image_set;
//all the rest are test images
	for (unordered_map<string, vector<path> >::iterator it = label_to_files_map.begin(); it != label_to_files_map.end(); it++) {
		const string label = it->first;
		vector < path > *files = &label_to_files_map[label];
		test_image_set.insert(files->begin(), files->end());
	}

	test_images.insert(test_images.end(), test_image_set.begin(), test_image_set.end());
	
	
	// thsi is to be deleted 
	path p1;
	path p2;
	DataSplit data_split = DataSplit(p1, p2, "", "");
	return data_split;
}


unordered_map<string, vector<path> > DataSplitter::get_files_per_label(path& image_dir, string& ground_truth_suffix, string& image_format, unordered_map<RGB, string>& colors_for_labels) {

	unordered_map<string, vector<path> > label_to_files_map;
	//initialise label_to_files_map
	for (unordered_map<RGB, string>::iterator it = colors_for_labels.begin(); it != colors_for_labels.end(); it++) {
		label_to_files_map[it->second] = vector<path>();
	}

	directory_iterator end_itr; //default construction provides an end reference
	RGB rgb(0, 0, 0); //default rgb struct, values will be changed in the loop
	unordered_map<string, int> labels_present;

	for (directory_iterator itr(image_dir); itr != end_itr; itr++) {

		path file = itr->path();
		string filename = file.filename().string();

		if (extension(file) == image_format && filename.find(ground_truth_suffix) != string::npos) {
			//ground truth file
			Mat img = imread(file.string(), CV_LOAD_IMAGE_COLOR);
			labels_present.clear();

			for (int y = 0; y < img.rows; y++) {
				for (int x = 0; x < img.cols; x++) {
					//gives data as bgr
					Point3_<uchar>* pixel = img.ptr<Point3_<uchar> >(y, x);
					rgb.red = (int) pixel->z;
					rgb.green = (int) pixel->y;
					rgb.blue = (int) pixel->x;
					const string label = colors_for_labels[rgb];
					if (labels_present.find(label) == labels_present.end()) {
						//unseen label in this image
						label_to_files_map[label].push_back(remove_from_filename(file, ground_truth_suffix));
						labels_present[label] = 1;
					}
				}
			}
		}
	}
	return label_to_files_map;
}

path DataSplitter::remove_from_filename(path& file, string& str){
	string filename = file.filename().string();
	replace(filename, str, "");
	file.remove_leaf();
	file /= filename;
	return file;
}
