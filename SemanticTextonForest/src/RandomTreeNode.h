/*
 * RTNode.h
 *
 *  Created on: 11 Oct 2012
 *      Author: Aly
 */

#ifndef RANDOMTREENODE_H_
#define RANDOMTREENODE_H_

template<class DataPoint, typename Functor>
class RandomTreeNode {
private:
	RandomTreeNode<DataPoint, Functor> *left;
	RandomTreeNode<DataPoint, Functor> *right;
	double threshold;
	bool is_a_leaf;
	std::vector<DataPoint> *data;
	int functor_id;
	double* class_dist;
	int number_of_classes;
	
public:
	
	RandomTreeNode(int number_of_classes): left(0), right(0), threshold(0.0), is_a_leaf(false), data(0), functor_id(-1), 
	class_dist(0), number_of_classes(number_of_classes){}

	void set_functor_id(int functor_id){this->functor_id = functor_id;}
	int get_functor_id() const { return this->functor_id;}
	
	void set_threshold(double threshold){this->threshold = threshold;}
	double get_threshold() const {return threshold;}
	
	void create_left_child(){ left = new RandomTreeNode<DataPoint, Functor>(number_of_classes);}
//	returning references so that they can be altered in a recursive tree build algo without excessive copying
	RandomTreeNode& get_left_child(){return *left;}
	
	void create_right_child(){ right = new RandomTreeNode<DataPoint, Functor>(number_of_classes);}
	RandomTreeNode& get_right_child(){return *right;}
	
	bool is_leaf() const {return this->is_a_leaf;}
	void mark_as_leaf(){this->is_a_leaf = true;}
	
	std::vector<DataPoint>& get_data() {
		return *data;
	}
	
	double* get_class_distribution(){
		return class_dist;
	}
	
	void set_data(std::vector<DataPoint>& data){
		this->data = new std::vector<DataPoint>(data);
		//create the histogram
		if(class_dist != 0){
			delete class_dist;
		}

		class_dist = new double[number_of_classes];
		std::fill_n(class_dist, number_of_classes, 0);
		
		double total_sum = 0;
		for(typename std::vector<DataPoint>::iterator it = this->data->begin(); it != this->data->end(); it++){
			total_sum += it->get_class_weight();
			class_dist[it->get_class_label()] += it->get_class_weight(); 
		}
		
		for(int i = 0; i < number_of_classes; i++){
			class_dist[i] /= total_sum;
		}
		
	}

	~RandomTreeNode(){
		delete right;
		delete left;
		delete data;
		if(class_dist != 0){
			delete class_dist;
		}
	}
};


#endif /* RANDOMTREENODE_H_ */
