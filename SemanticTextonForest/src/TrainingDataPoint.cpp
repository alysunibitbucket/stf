/*
 * TrainingDataPoint.cpp
 *
 *  Created on: 11 Oct 2012
 *      Author: Aly
 */

#include "TrainingDataPoint.h"
#include "ImagePoint.h"

using namespace std;

TrainingDataPoint::TrainingDataPoint(int row, int col, int class_label, int patch_top_left_row, 
		int patch_top_left_col, int patch_size_rows, int patch_size_cols, int image_hash_key, string image_filename)
		: row(row), col(col), class_label(class_label), patch_top_left_row(patch_top_left_row), patch_top_left_col(patch_top_left_col),
		  patch_size_rows(patch_size_rows), patch_size_cols(patch_size_cols), image_hash_key(image_hash_key), 
		  image_filename(image_filename), class_weight(0){};

int TrainingDataPoint::get_class_label() const{
	return this->class_label;
}

int TrainingDataPoint::get_image_hash_key() const{
	return this->image_hash_key;
}

void TrainingDataPoint::set_class_weight(double w){
	class_weight = w;
}

double TrainingDataPoint::get_class_weight(){
	return class_weight;
}

string TrainingDataPoint::get_image_filename() const {return image_filename;}

int TrainingDataPoint::get_patch_top_left_row() const {return patch_top_left_row;}
int TrainingDataPoint::get_patch_top_left_col() const {return patch_top_left_col;}
int TrainingDataPoint::get_patch_size_rows() const {return patch_size_rows;}
int TrainingDataPoint::get_patch_size_cols() const {return patch_size_cols;}
int TrainingDataPoint::get_row() const {return row;}
int TrainingDataPoint::get_col() const {return col;}
