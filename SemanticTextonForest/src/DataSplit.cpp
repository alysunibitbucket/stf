/*
 * DataSplit.cpp
 *
 *  Created on: 12 Oct 2012
 *      Author: Aly
 */

#include "DataSplit.h"
#include <boost/filesystem.hpp>
#include <vector>

using namespace std;
using namespace boost::filesystem;

DataSplit::DataSplit(path training_image_dir, path test_image_dir, string ground_truth_suffix, string image_format) {
	this->test_image_files = get_image_files(test_image_dir, ground_truth_suffix, image_format, false);
	this->training_image_files = get_image_files(training_image_dir, ground_truth_suffix, image_format, true);
}

vector<path> DataSplit::get_image_files(path& image_dir, string& ground_truth_suffix, string& image_format, bool ground_truth_files ){
	vector<path> image_files;
	
	directory_iterator end_itr; //default construction provides an end reference
	for (directory_iterator itr(image_dir); itr != end_itr; itr++) {
		path file = itr->path();
		string filename = file.filename().string();
		if(extension(file) == image_format && filename.find(ground_truth_suffix) == string::npos)
		{
			image_files.push_back(file);
		}
	}
	
	return image_files;
}

vector<path> DataSplit::get_test_image_files(){
	return this->test_image_files;
}
vector<path> DataSplit::get_training_image_files(){
	return this->training_image_files;
}
