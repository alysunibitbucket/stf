/*
 * TrainingDataPointGenerator.cpp
 *
 *  Created on: 15 Oct 2012
 *      Author: Aly
 */

#include "TrainingDataPointGenerator.h"
#include "TrainingDataPoint.h"
#include <vector>
#include <boost/filesystem.hpp>
#include <boost/unordered_map.hpp>
#include "utils.hpp"
#include "RGB.h"
#include "ImageSampler.h"

using namespace std;
using namespace boost::filesystem;
using namespace boost;

TrainingDataPointGenerator::TrainingDataPointGenerator() {
}

vector<TrainingDataPoint> TrainingDataPointGenerator::get_training_data_points(vector<path>& training_image_files, 
		unordered_map<string, int> filename_to_hash_key_map,
		string& ground_truth_suffix, unordered_map<RGB, int>& colors_for_labels, int sample_frequency, int patch_size_rows, int patch_size_cols){

	vector<TrainingDataPoint> all_data_points;
	ImageSampler image_sampler = ImageSampler();

	for(vector<path>::iterator file = training_image_files.begin(); file != training_image_files.end(); file++){
			path ground_truth_file = this->get_ground_truth_file(*file, ground_truth_suffix);
			cout << ground_truth_file.string() << endl;
			vector<TrainingDataPoint> data_points_for_image = image_sampler.sample_data_points(*file, ground_truth_file, sample_frequency, patch_size_rows, patch_size_cols, colors_for_labels, filename_to_hash_key_map);
			all_data_points.insert(all_data_points.end(), data_points_for_image.begin(), data_points_for_image.end());
	}
		
	
	return all_data_points;
}


path TrainingDataPointGenerator::get_ground_truth_file(const path& file, string ground_truth_suffix){
	string filename = file.filename().string();
	string extension = file.extension().filename().string();
	replace(filename, extension, ground_truth_suffix.append(extension.begin(), extension.end()));
	path new_file = path(file);
	new_file.remove_leaf();
	new_file /= filename;
	return new_file;
}
