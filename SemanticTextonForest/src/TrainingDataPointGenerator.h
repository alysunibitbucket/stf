/*
 * TrainingDataPointGenerator.h
 *
 *  Created on: 15 Oct 2012
 *      Author: Aly
 */

#ifndef TRAININGDATAPOINTGENERATOR_H_
#define TRAININGDATAPOINTGENERATOR_H_

#include <vector>
#include "TrainingDataPoint.h"
#include <boost/filesystem.hpp>
#include <boost/unordered_map.hpp>
#include "RGB.h"

class TrainingDataPointGenerator {
private:
	boost::filesystem::path get_ground_truth_file(const boost::filesystem::path& file, const std::string ground_truth_suffix);
public:
	TrainingDataPointGenerator();
	std::vector<TrainingDataPoint> get_training_data_points(std::vector<boost::filesystem::path>& training_image_files,
			boost::unordered_map<std::string, int> filename_to_hash_key_map,
			std::string& ground_truth_suffix, boost::unordered_map<RGB, int>& colors_for_labels, int sample_frequency, int patch_size_rows, int patch_size_cols);
};

#endif /* TRAININGDATAPOINTGENERATOR_H_ */
