/*
 * RandomTreeFunction.h
 *
 *  Created on: 17 Oct 2012
 *      Author: Aly
 */

#ifndef STFRANDOMTREEFUNCTION_H_
#define STFRANDOMTREEFUNCTION_H_

#include "STFDataPoint.h"
#include "utils.hpp"
#include <opencv2/opencv.hpp>
#include <boost/unordered_map.hpp>


struct SoftLabelRandomTreeFunction
{
    typedef double (*function_ptr)(const int& patch_top_left_row, const int& patch_top_left_col, const int& image_hash_key,
    		std::vector<cv::Mat>* preloaded_images,
    		int* random_parameters);
};

struct SoftLabelRandomParameterGenerator{
	typedef int* (*function_ptr)(int patch_size_rows, int patch_size_cols);
};


struct SoftLabelRandomTreeFunctor
{
private:
	std::vector<cv::Mat>* image_cache;
	SoftLabelRandomTreeFunction::function_ptr function;
	int* random_values;
	int functor_id;
	
	
public:
	SoftLabelRandomTreeFunctor(){};
	SoftLabelRandomTreeFunctor(std::vector<cv::Mat>* cache, SoftLabelRandomTreeFunction::function_ptr function,
			int functor_id, int* random_values)
	:image_cache(cache), function(function)
	, random_values(random_values), functor_id(functor_id){}
	
	int get_functor_id(){
		return functor_id;
	}
	
	void set_functor_id(int f_id){
		functor_id = f_id;
	}
	
	
	
	double operator()(TrainingDataPoint& data_point){
		
		if(image_cache == 0){
			std::cerr << "Trying to call function without valid image cache" << std::endl;
			exit(-1);
		}
		return this->operator ()(data_point.get_patch_top_left_row(), data_point.get_patch_top_left_col(), data_point.get_image_hash_key());
	}
	
	//This version has been put in for test time for speed, during training the generic random tree class uses the version
	//above
	double operator()(int patch_top_left_row, int patch_top_left_col, int image_hash_key){
		if(image_cache == 0){
			std::cerr << "Trying to call function without valid image cache" << std::endl;
			exit(-1);
		}
		
		
		return function(patch_top_left_row, patch_top_left_col, image_hash_key, image_cache, random_values);
	}
	
	void set_image_cache(std::vector<cv::Mat>* cache ){
		image_cache = cache;	
	}
	
	void delete_image_cache(){
		image_cache = 0;
	}
	
	std::vector<cv::Mat>* get_image_cache(){
		return image_cache;
	}
	
		
	~SoftLabelRandomTreeFunctor(){
	}
};



struct RegionPriorTreeFunctor{
public:
	
	typedef boost::multi_array<float, 3> region_prior_integral_image;
	std::vector<region_prior_integral_image*>* image_cache;
	int functor_id;
	int img_rows;
	int img_cols;
	int offset_rows;
	int offset_cols;
	int rect_rows;
	int rect_cols;
	int class_index;

	RegionPriorTreeFunctor(){};
	RegionPriorTreeFunctor(std::vector<region_prior_integral_image*>* image_cache, int functor_id, 
			int img_rows, int img_cols, 
			int offset_rows, int offset_cols, 
			int rect_rows, int rect_cols,
			int class_index)
	:image_cache(image_cache), functor_id(functor_id),
	 img_rows(img_rows), img_cols(img_cols),
	 offset_rows(offset_rows), offset_cols(offset_cols),
	 rect_rows(rect_rows), rect_cols(rect_cols),
	 class_index(class_index){};
	
	
	int get_functor_id(){
		return functor_id;
	}
	
	void set_functor_id(int f_id){
		this->functor_id = f_id;
	}
	
	void set_image_cache(std::vector<region_prior_integral_image*>* cache ){
		image_cache = cache;	
	}
		
	void delete_image_cache(){
		image_cache = 0;
	}
		
	std::vector<region_prior_integral_image*>* get_image_cache(){
		return image_cache;
	}


	//This is the function that gets called during training
	double operator()(TrainingDataPoint& data_point){
		if(image_cache == 0){
			std::cerr << "Trying to call function without valid image cache" << std::endl;
			exit(-1);
		}
		return this->operator ()(data_point.get_row(), data_point.get_col(), data_point.get_image_hash_key());
	}
		
	//This one is used for testing, and assumes the random values have been set
	double operator()(const int& row, const int& col , const int& image_hash_key){
		
		/*
		 * These rows and columns correspond to the original image dimensions
		 * The integral image has an extra row and col
		 * So if we are processing for (r,c) then in the integral
		 * image we need to check (r+1, c+1)
		 */
		
//		int ii_row = row + 1;
//		int ii_col = col + 1;
//		
//		
//		
//		region_prior_integral_image& rpii = *(image_cache->at(image_hash_key));
//		
//		
//		
//		int top_left_row = ii_row  + offset_rows;
//		if(top_left_row < 0){ top_left_row = 0;}
//		/*Usually we would say if >= img_row (as indexes start at 0)
//		 * but as the integral image has an extra row and col
//		 * we can look up img[720][960] which corresponds to
//		 * original[719][959] 
//		*/
//		if(top_left_row > img_rows) {top_left_row = img_rows;}
//		
//		int top_left_col = ii_col  + offset_cols;
//		if(top_left_col < 0){ top_left_col = 0;}
//		if(top_left_col > img_cols){ top_left_col = img_cols;}
//		
//		
//		int top_right_col = top_left_col + rect_cols;
//		if(top_right_col > img_cols) { top_right_col = img_cols ;}
//		
//		int bottom_left_row = top_left_row + rect_rows;
//		if(bottom_left_row > img_rows) { bottom_left_row = img_rows ; }
//		
//
//		int pixel_count_in_region = (top_right_col - top_left_col) * (bottom_left_row - top_left_row);
//		if(pixel_count_in_region == 0){
//			return 0;
//		}
//		
//		return ((rpii[bottom_left_row][top_right_col][class_index] + rpii[top_left_row][top_left_col][class_index]
//		       - rpii[top_left_row][top_right_col][class_index] - rpii[bottom_left_row][top_left_col][class_index])
//				/(double)pixel_count_in_region);
		
		return 0.5;
		
	}
	
	~RegionPriorTreeFunctor(){};
};

#endif /* STFRANDOMTREEFUNCTION_H_ */
