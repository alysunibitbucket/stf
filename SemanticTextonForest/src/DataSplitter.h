/*
 * DataOrganizer.h
 *
 *  Created on: 12 Oct 2012
 *      Author: Aly
 */

#ifndef DATAORGANIZER_H_
#define DATAORGANIZER_H_

#include <boost/unordered_map.hpp>
#include <boost/filesystem.hpp>
#include <vector>
#include "DataSplit.h"
#include "RGB.h"

class DataSplitter {
private:
	boost::unordered_map<std::string, std::vector<boost::filesystem::path> > get_files_per_label(boost::filesystem::path& image_dir, std::string& ground_truth_suffix, std::string& image_format, boost::unordered_map<RGB, std::string>& colors_for_labels);
	boost::filesystem::path remove_from_filename(boost::filesystem::path& file, std::string& str);
public:
	DataSplitter();
	DataSplit split_data_into_training_and_test(boost::filesystem::path& image_dir, std::string& ground_truth_suffix, std::string& image_format, double& percentage_of_images_to_train_per_label, boost::unordered_map<RGB, std::string>& colors_for_labels);
};

#endif /* DATAORGANIZER_H_ */
