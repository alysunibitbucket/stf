#include <string>
#include <cstdlib>
using namespace std;

bool replace(string& str, const string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

int get_random_number_in_range(int min, int max){
	return min + (rand() % (int)(max - min + 1));
}
