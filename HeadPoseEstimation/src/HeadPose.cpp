/*
 * HeadPose.cpp
 *
 *  Created on: 17 Dec 2012
 *      Author: aly
 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <pcl/point_types.h>
#include <boost/filesystem.hpp>
#include "PatchGenerator.h"
#include "utils.hpp"
#include "RandomTree.h"
#include "DiscriminativeRandomRegressionNode.h"
#include "DRRClassSplitCalculator.h"
#include <boost/shared_ptr.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include "DRRForest.h"

using namespace std;
using namespace cv;
using namespace pcl;
using namespace boost::filesystem;




int main(int argc, char* argv[]) {
	
	path gt_pose_dir = path(argv[1]);
	path depth_dir = path(argv[2]);
	path mask_dir = path(argv[3]);
	if (!exists(gt_pose_dir) || !exists(depth_dir) || !exists(mask_dir)) {
		cerr << " directories do  not exist, exiting" << endl;
		exit(-1);
	}			
	
	DRRForest forest = DRRForest(5, 15, gt_pose_dir, depth_dir, mask_dir);
//	PatchGenerator pg = PatchGenerator(gt_pose_dir, depth_dir, mask_dir,100, 100,  10);
//	cout << pg.getNumberOfDetphFiles() << endl;
	
//
//	DRRClassSplitCalculator cl = DRRClassSplitCalculator();
////	vector<ImagePatch>& data, vector<DRRFunctor>& functors, int number_of_functors_to_try, int number_of_thresholds_to_try
//	vector<ImagePatch> data;
//	vector<DRRFunctor> functors;
//	
//	data_split<ImagePatch, DRRFunctor> a = cl.getBestDataSplit(data, functors, 1, 2);
	
	return 0;
}






