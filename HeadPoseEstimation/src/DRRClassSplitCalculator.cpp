/*
 * DRRClassSplitCalculator.cpp
 *
 *  Created on: 18 Dec 2012
 *      Author: aly
 */

#include "DRRClassSplitCalculator.h"
#include <vector>
#include "DiscriminativeRandomRegressionNode.h"
#include "ImagePatch.h"

using namespace std;
using namespace boost::random;
using namespace boost;




DRRClassSplitCalculator::DRRClassSplitCalculator(): random_number_generator(rng(time(0))) {
	// TODO Auto-generated constructor stub
}

data_split<ImagePatch, DRRFunctor> DRRClassSplitCalculator::getBestDataSplit(vector<ImagePatch>& data, vector<DRRFunctor>& functors, int number_of_functors_to_try, int number_of_thresholds_to_try)
{
	
	typedef boost::random::normal_distribution<double> gaussian;
	typedef boost::variate_generator<rng&, gaussian > variate_generator;	
	
	data_split<ImagePatch, DRRFunctor> best_data_split;
	
	for(int i = 0; i < number_of_functors_to_try; i++){
		DRRFunctor functor = get_random_item(functors);
						
		vector<pair<double, ImagePatch> > value_data_pairs = compute_value_data_pairs(data, functor);
				
		gaussian distribution = create_gaussian_from_samples(value_data_pairs); 

		variate_generator var_gen = variate_generator(random_number_generator, distribution);
		
		double best_regression_uncertainty = DBL_MAX;
		
		for(int j = 0; j < number_of_thresholds_to_try; j++){
		
			double threshold = var_gen();
			vector<ImagePatch > left_split;
			vector<ImagePatch > right_split;
							
			for(typename vector<pair<double, ImagePatch> >::iterator it = value_data_pairs.begin(); it < value_data_pairs.end(); it++){
				if(it->first <= threshold){
					left_split.push_back(it->second);
				}
				else{
					right_split.push_back(it->second);
				}
			}
			
			double regression_uncertainty = calculate_regression_uncertainty(left_split, right_split); 
			if(regression_uncertainty < best_regression_uncertainty){
				best_data_split = data_split<ImagePatch, DRRFunctor>(left_split, right_split, functor, threshold);
			}
		}
	}
	
	return best_data_split;
}


double DRRClassSplitCalculator::calculate_regression_uncertainty(vector<ImagePatch>& left_split, vector<ImagePatch>& right_split){
	double regression_uncenrtainty = 0;
	
	return regression_uncenrtainty;
	
	matrix left_offset_vector_cov = calculate_offset_vector_covariance_matrix(left_split);
	matrix left_angle_vector_cov = calculate_angle_vector_covariance_matrix(left_split);
	
	matrix right_offset_vector_cov = calculate_offset_vector_covariance_matrix(right_split);
	matrix right_angle_vector_cov = calculate_angle_vector_covariance_matrix(right_split);
	
	double total_patches = left_split.size() + right_split.size();
	double left_weight = ((double)left_split.size())/total_patches;
	double right_weight = ((double)right_split.size())/total_patches;
	
	return  (left_weight* (determinant(left_angle_vector_cov) + determinant(left_offset_vector_cov)))
			+( right_weight * (determinant(right_angle_vector_cov) + determinant(right_offset_vector_cov)));
}

DRRClassSplitCalculator::matrix DRRClassSplitCalculator::calculate_angle_vector_covariance_matrix(vector<ImagePatch>& data){
	matrix angle_vector_covariance_matrix = matrix(3,3);
	double* means = calculate_angle_vector_sample_means(data);
	
	//q_jk = 1/n-1 sum(i=0..n)(x_ij - x_-j)(xik - x_-k)
	int size = data.size();
	
	for(int j = 0; j < 3; j++){
		for(int k = 0; k < 3; k++){
			for(vector<ImagePatch>::iterator i = data.begin(); i != data.end(); i++){
//				double* angle_pose = i->get_angle_pose();
//				angle_vector_covariance_matrix(j,k) += (angle_pose[j] - means[j]) * (angle_pose[k] - means[k]);  	
			}
		}
	}
	
	for(int j = 0; j < 3; j++){
		for(int k = 0; k < 3; k++){
			angle_vector_covariance_matrix(j,k) /= (double)(size - 1);
		}
	}
	
	return angle_vector_covariance_matrix;	
}


DRRClassSplitCalculator::matrix DRRClassSplitCalculator::calculate_offset_vector_covariance_matrix(vector<ImagePatch>& data){
	matrix offset_vector_covariance_matrix = matrix(3,3);
	double* means = calculate_offset_vector_sample_means(data);
	
	//q_jk = 1/n-1 sum(i=0..n)(x_ij - x_-j)(xik - x_-k)
	int size = data.size();
	
	for(int j = 0; j < 3; j++){
		for(int k = 0; k < 3; k++){
			for(vector<ImagePatch>::iterator i = data.begin(); i != data.end(); i++){
//				double* centre_pose = i->get_centre_pose();
//				offset_vector_covariance_matrix(j,k) += (centre_pose[j] - means[j]) * (centre_pose[k] - means[k]);  	
			}
		}
	}
	
	for(int j = 0; j < 3; j++){
		for(int k = 0; k < 3; k++){
			offset_vector_covariance_matrix(j,k) /= (double)(size - 1);
		}
	}
	
	return offset_vector_covariance_matrix;
	
}
double* DRRClassSplitCalculator::calculate_angle_vector_sample_means(vector<ImagePatch>& data){
	double pitch_bar = 0;
	double yaw_bar = 0;
	double roll_bar = 0;
	for(vector<ImagePatch>::iterator it = data.begin(); it != data.end(); it++){
		pitch_bar += it->get_pitch();
		yaw_bar += it->get_yaw();
		roll_bar += it->get_roll();
	}
	
	
	double means[3];
	double size = (double)data.size();
	means[0] = (double)pitch_bar/size;
	means[1] = (double)yaw_bar/size;
	means[2] = (double)roll_bar/size;
	return means;
	
}
double* DRRClassSplitCalculator::calculate_offset_vector_sample_means(vector<ImagePatch>& data){
	//x_j = 1/n sum(i=1...n)x_ij
	
	double x_bar = 0;
	double y_bar = 0;
	double z_bar = 0;
	for(vector<ImagePatch>::iterator it = data.begin(); it != data.end(); it++){
		x_bar += it->get_head_centre_x();
		y_bar += it->get_head_centre_y();
		z_bar += it->get_head_centre_z();
	}
	
	
	double means[3];
	double size = (double)data.size();
	means[0] = (double)x_bar/size;
	means[1] = (double)y_bar/size;
	means[2] = (double)z_bar/size;
	return means;
}



DRRClassSplitCalculator::~DRRClassSplitCalculator(){}
