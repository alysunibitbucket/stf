/*
 * ImagePatch.cpp
 *
 *  Created on: 17 Dec 2012
 *      Author: aly
 */

#include "ImagePatch.h"
#include <pcl/point_types.h>

using namespace pcl;
using namespace std;

ImagePatch::ImagePatch(int class_type, PointXYZ head_centre_point, float yaw, float pitch, float roll, string person_id, int depth_file_key, PointXYZ patch_centre_point)
:class_type(class_type), head_centre_point(head_centre_point), pitch(pitch), yaw(yaw), 
 roll(roll), person_id(person_id), depth_file_key(depth_file_key), patch_centre_point(patch_centre_point)
{
		
}


ImagePatch::~ImagePatch() {

}
