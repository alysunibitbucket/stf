////============================================================================
//// Name        : ObjRec.cpp
//// Author      : Alykhan Tejani
//// Version     :
//// Copyright   : LGPL
//// Description : Hello World in C++, Ansi-style
////============================================================================
//
//#include <iostream>
//#include <boost/filesystem.hpp>
//#include <pcl/io/pcd_io.h>
//#include <pcl/point_types.h>
//#include <pcl/kdtree/kdtree_flann.h>
//#include <pcl/kdtree/kdtree.h>
//#include <pcl/io/io.h>
//#include <opencv2/opencv.hpp>
//#include <opencv2/contrib/contrib.hpp>
//#include <pcl/visualization/cloud_viewer.h>
//#include <pcl/visualization/pcl_visualizer.h>
//#include <pcl/point_types.h>
//#include <pcl/features/normal_3d.h>
//#include <pcl/features/integral_image_normal.h>
//#include <vtk-5.8/vtkSmartPointer.h>
//#include <pcl/common/transforms.h>
//#include <boost/timer.hpp>
//#include <boost/thread/thread.hpp>
//
//using namespace std;
//using namespace boost;
//using namespace boost::filesystem;
//using namespace cv;
//
//
//pcl::PointCloud<pcl::PointXYZRGB>::Ptr create_point_cloud_ptr(Mat& depthImage, Mat& rgbImage){
//	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
//	cloud->width = depthImage.cols; //Dimensions must be initialized to use 2-D indexing
//	cloud->height = depthImage.rows;
//	cloud->resize(cloud->width*cloud->height);
//	
//	cout << " have set cloud height to " << cloud->height << endl;
//	int num_of_points_added = 0;
//	for(int v=0; v< depthImage.rows; v++){ //2-D indexing
//		for(int u=0; u< depthImage.cols; u++) {
//			if(depthImage.at<uint16_t>(v,u) != 0){
//				Vec3b bgrPixel = rgbImage.at<Vec3b>(v, u);
//				pcl::PointXYZRGB p = pcl::PointXYZRGB();
//				p.b = bgrPixel[0];
//				p.g = bgrPixel[1];
//				p.r = bgrPixel[2];
//				p.x = u;
//				p.y = v;
//				p.z = depthImage.at<uint16_t>(v,u);
//				cloud->push_back(p);
//				num_of_points_added++;
//			}
//		}
//	}
//	cout <<" num of points added = " << num_of_points_added << endl;
//	return cloud;
//}
//
///*
// */
//int main(int argc, char* argv[]) {
//	
//	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
//
//	if (pcl::io::loadPCDFile<pcl::PointXYZRGB> ("coffee_mug_1_1_66.pcd", *cloud) == -1) //* load the file
//	{
//		PCL_ERROR ("Couldn't read file coffee_mug_1_1_66.pcd \n");
//	    cin.get();
//	    return (-1);
//	 }
//	      std::cout << "Loaded "
//	                << cloud->width * cloud->height
//	                << " data points from test_pcd.pcd with the following fields: "
//	                << std::endl;
//
//	
//	//	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud = create_point_cloud_ptr(depthImage, rgbImage);      
//	
//
//	      for(pcl::PointCloud<pcl::PointXYZRGB>::iterator it = cloud->begin(); it != cloud->end(); it++){
//	      	if(it->y <0){
//	      		cout << it->x << ", " << it->y << ", " << it->z << endl;
//	      	}
//	      	
//	      	swap(it->z, it->y);
//	      	
//	      }	
//	      	  
//	      Eigen::Matrix4f transformationMatrix;
//	      
//	      transformationMatrix << cos(-30.0), -1*sin(-30.0), 0.0, 0.0,
//	      				sin(-30.0), cos(-30.0), 0.0, 0.0,
//	      				0.0, 0.0, 1.0, 0.0,
//	      				0.0, 0.0, 0.0, 1.0;
//	      		
//	      
//	      pcl::transformPointCloud(*cloud, *cloud, transformationMatrix);
//	      
//	      
//	      
//	pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
//	ne.setInputCloud (cloud);
//	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB> ());
//	ne.setSearchMethod (tree);
//	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals1 (new pcl::PointCloud<pcl::Normal>);
//	ne.setRadiusSearch (0.005); // radius search in 1/2 cm
//    ne.compute (*cloud_normals1);
//	      
//    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
//    viewer->setBackgroundColor (0, 0, 0);
//    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
//    viewer->addPointCloud<pcl::PointXYZRGB> (cloud,rgb, "sample cloud");
//    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
//    viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal> (cloud, cloud_normals1, 10, 0.01, "normals");
//    viewer->addCoordinateSystem (1.0);
//    viewer->initCameraParameters ();
//    
//    
//    //	//TODO:
//    //	/*
//    //	 * check point.size match the pixels in the seg mask
//    //	 * 
//    //	 */
//    //	
//    //	Mat depthImage;
//    //	Mat segmentationMask;
//    //	Mat rgbImage;
//    //	depthImage = imread("coffee_mug_1_1_1_depthcrop.png", CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR ); // Read the file
//    //	segmentationMask = imread("coffee_mug_1_1_1_maskcrop.png", CV_LOAD_IMAGE_GRAYSCALE);
//    //	rgbImage = imread("coffee_mug_1_1_1_crop.png", CV_LOAD_IMAGE_COLOR);
//    //	
//    //	int number_of_segmented_pixels = 0;
//    //	for(int i = 0; i < depthImage.rows; i++){
//    //		for(int j = 0; j < depthImage.cols; j++){
//    //			if(segmentationMask.at<uchar>(i,j) == 0){
//    //				depthImage.at<uint16_t>(i,j) = (uint16_t)0;
//    //			}
//    //			else{
//    //				number_of_segmented_pixels++;
//    //			}
//    //		}
//    //	}
//    //	int non_zeros_in_depth = 0;
//    //	for(int i = 0; i < depthImage.rows; i++){
//    //		for(int j = 0; j < depthImage.cols; j++){
//    //			if(depthImage.at<uint16_t>(i,j) == 0){
//    //				Vec3b& vec_bgr = rgbImage.at<Vec3b>(i,j);
//    //				vec_bgr[0] = 0;
//    //				vec_bgr[1] = 0;
//    //				vec_bgr[2] = 0;
//    //			}
//    //			else{
//    //				non_zeros_in_depth ++;
//    //			}
//    //		}
//    //	}
//    //
//    //
//    //			double min;
//    //			double max;
//    //			minMaxIdx(depthImage, &min, &max);
//    //			Mat adjustedImage;
//    //			convertScaleAbs(depthImage, adjustedImage, 255 / max);
//    //			namedWindow("Out");
//    //			namedWindow("rgb");
//    //			
//    //			imshow("rgb", rgbImage);
//    //			imshow("Out", adjustedImage);
//    //			waitKey(0);
//    //	
//    	
//
//
//      
//    while (!viewer->wasStopped ())
//    {
//      viewer->spinOnce(100);
//      boost::this_thread::sleep (boost::posix_time::microseconds (100000));
//    }
//	return 0;
//}
//
////	//TODO:
////	/*
////	 * check point.size match the pixels in the seg mask
////	 * 
////	 */
////	
////	Mat depthImage;
////	Mat segmentationMask;
////	Mat rgbImage;
////	depthImage = imread("coffee_mug_1_1_1_depthcrop.png", CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR ); // Read the file
////	segmentationMask = imread("coffee_mug_1_1_1_maskcrop.png", CV_LOAD_IMAGE_GRAYSCALE);
////	rgbImage = imread("coffee_mug_1_1_1_crop.png", CV_LOAD_IMAGE_COLOR);
////	
////	int number_of_segmented_pixels = 0;
////	for(int i = 0; i < depthImage.rows; i++){
////		for(int j = 0; j < depthImage.cols; j++){
////			if(segmentationMask.at<uchar>(i,j) == 0){
////				depthImage.at<uint16_t>(i,j) = (uint16_t)0;
////			}
////			else{
////				number_of_segmented_pixels++;
////			}
////		}
////	}
////	int non_zeros_in_depth = 0;
////	for(int i = 0; i < depthImage.rows; i++){
////		for(int j = 0; j < depthImage.cols; j++){
////			if(depthImage.at<uint16_t>(i,j) == 0){
////				Vec3b& vec_bgr = rgbImage.at<Vec3b>(i,j);
////				vec_bgr[0] = 0;
////				vec_bgr[1] = 0;
////				vec_bgr[2] = 0;
////			}
////			else{
////				non_zeros_in_depth ++;
////			}
////		}
////	}
////
////
////			double min;
////			double max;
////			minMaxIdx(depthImage, &min, &max);
////			Mat adjustedImage;
////			convertScaleAbs(depthImage, adjustedImage, 255 / max);
////			namedWindow("Out");
////			namedWindow("rgb");
////			
////			imshow("rgb", rgbImage);
////			imshow("Out", adjustedImage);
////			waitKey(0);
////	
//	
