/*
 * utils.cpp
 *
 *  Created on: 18 Dec 2012
 *      Author: aly
 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <opencv2/opencv.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

using namespace cv;
/*
 * returns pose as array {x,y,z,pitch,yaw,roll}
 */
float* readGroundTruthPose(const string& filename){

	const char* fname = filename.c_str();
	//try to read in the ground truth from a binary file
	FILE* pFile = fopen(fname, "rb");
	if(!pFile){
		std::cerr << "could not open file " << fname << std::endl;
		return NULL;
	}
	
	float* data = new float[6];
	
	bool success = true;
	success &= ( fread( &data[0], sizeof(float), 6, pFile) == 6 );
	fclose(pFile);
	
	if(success)
		return data;
	else{
		delete [] data;
		return NULL;
	}
}

Mat loadDepthImageCompressed( const string& filename){

	const char* fname = filename.c_str();
	//now read the depth image
	FILE* pFile = fopen(fname, "rb");
	if(!pFile){
		std::cerr << "could not open file " << fname << std::endl;
		return Mat();
	}

	int im_width = 0;
	int im_height = 0;
	
	
	bool success = true;

	success &= ( fread(&im_width,sizeof(int),1,pFile) == 1 ); // read width of depthmap
	success &= ( fread(&im_height,sizeof(int),1,pFile) == 1 ); // read height of depthmap

	int16_t* depth_img = new int16_t[im_width*im_height];

	int numempty;
	int numfull;
	int p = 0;

	while(p < im_width*im_height ){

		success &= ( fread( &numempty,sizeof(int),1,pFile) == 1 );

		for(int i = 0; i < numempty; i++)
			depth_img[ p + i ] = 0;

		success &= ( fread( &numfull,sizeof(int), 1, pFile) == 1 );
		success &= ( fread( &depth_img[ p + numempty ], sizeof(int16_t), numfull, pFile) == (unsigned int) numfull );
		p += numempty+numfull;

	}

	fclose(pFile);

	if(success){
		Mat depth_img_meters = Mat(480, 640, CV_32F);
		
		for(int row = 0; row < im_height; row++){
			for(int col = 0; col < im_width; col++){
				depth_img_meters.at<float>(row, col) = (int16_t) depth_img[(640*row + col)]  * 0.001;
			}
		}
		
		return depth_img_meters;
	}
		
	else{
		delete [] depth_img;
		return Mat();
	}
}

void showDepthImage(Mat& depth_img){
	double min;
	double max;
	minMaxIdx(depth_img, &min, &max);
	Mat adjustedImage;
	convertScaleAbs(depth_img, adjustedImage, 255 / max);
	namedWindow("Out", CV_WINDOW_AUTOSIZE);
	
	imshow("Out", depth_img);
	waitKey(0);
}

//returns in range [min, max]
int get_random_number_in_range(int min, int max){
	return min + (rand() % (int)(max - min + 1));
}


int determinant_sign(const boost::numeric::ublas::permutation_matrix<std ::size_t>& pm)
{
    int pm_sign=1;
    std::size_t size = pm.size();
    for (std::size_t i = 0; i < size; ++i)
        if (i != pm(i))
            pm_sign *= -1.0; // swap_rows would swap a pair of rows here, so we change sign
    return pm_sign;
}
 
double determinant( boost::numeric::ublas::matrix<double>& m ) {
    boost::numeric::ublas::permutation_matrix<std ::size_t> pm(m.size1());
    double det = 1.0;
    if( boost::numeric::ublas::lu_factorize(m,pm) ) {
        det = 0.0;
    } else {
        for(int i = 0; i < m.size1(); i++)
            det *= m(i,i); // multiply by elements on diagonal
        det = det * determinant_sign( pm );
    }
    return det;
}
 


