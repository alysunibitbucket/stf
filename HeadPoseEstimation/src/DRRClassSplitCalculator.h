/*
 * DRRClassSplitCalculator.h
 *
 *  Created on: 18 Dec 2012
 *      Author: aly
 */

#ifndef DRRCLASSSPLITCALCULATOR_H_
#define DRRCLASSSPLITCALCULATOR_H_
#include "RandomTree.h"
#include "ImagePatch.h"
#include "DiscriminativeRandomRegressionNode.h"
#include <vector>
#include "utils.hpp"
#include <boost/random/normal_distribution.hpp>
#include <boost/random.hpp>

class DRRClassSplitCalculator {
private:
	typedef boost::mt19937 rng;
	rng random_number_generator;
	typedef boost::numeric::ublas::matrix<double> matrix;
	
	double calculate_regression_uncertainty(std::vector<ImagePatch>& left_split, std::vector<ImagePatch>& right_split);
	
	matrix calculate_offset_vector_covariance_matrix(std::vector<ImagePatch>& data);
	matrix calculate_angle_vector_covariance_matrix(std::vector<ImagePatch>& data);
	
	double* calculate_offset_vector_sample_means(std::vector<ImagePatch>& data);
	double* calculate_angle_vector_sample_means(std::vector<ImagePatch>& data);
	
public:
	DRRClassSplitCalculator();
	data_split<ImagePatch, DRRFunctor> getBestDataSplit(std::vector<ImagePatch>& data, std::vector<DRRFunctor>& functors, 
														 int number_of_functors_to_try, int number_of_thresholds_to_try);	
	virtual ~DRRClassSplitCalculator();
};

#endif /* DRRCLASSSPLITCALCULATOR_H_ */
