/*
 * DRRForest.cpp
 *
 *  Created on: 19 Dec 2012
 *      Author: aly
 */

#include "DRRForest.h"
#include <vector>
#include <opencv2/opencv.hpp>
#include "PatchGenerator.h"
#include "utils.hpp"
#include "RandomTree.h"
#include "DiscriminativeRandomRegressionNode.h"
#include "DRRClassSplitCalculator.h"
#include <boost/shared_ptr.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/unordered_map.hpp>
#include "utils.hpp"
#include <boost/filesystem.hpp>
#include "PatchGenerator.h"


using namespace std;
using namespace boost::filesystem;
using namespace boost;
using namespace cv;


double drr_rectangle_function(const int& patch_centre_x, const int& patch_centre_y, 
	   	   	   	   Rectangle& rec1, Rectangle& rec2, const int& channel, const int& image_hash_key,
	   	   	   	   boost::shared_ptr<img_matrix> image_cache){

	
	Mat& integral_image = image_cache->at_element(image_hash_key, channel);
	int rec1_bottom_right_x = patch_centre_x - rec1.top_left_x_offset_from_centre + rec1.width;
	int rec1_bottom_right_y = patch_centre_y - rec1.top_left_y_offset_from_centre + rec1.height;
	
	//integral image: S(x,y) = i(x,y) - i(x - width,y) - i(x, y - height) + i(x-width, y-height) 
	double rec1_sum = integral_image.at<float>(rec1_bottom_right_x, rec1_bottom_right_y) 
					- integral_image.at<float>(rec1_bottom_right_x, rec1_bottom_right_y - rec1.height)
					- integral_image.at<float>(rec1_bottom_right_x - rec1.width, rec1_bottom_right_y)
					+ integral_image.at<float>(rec1_bottom_right_x - rec1.width, rec1_bottom_right_y - rec1.height);
	
	int rec2_bottom_right_x = patch_centre_x - rec2.top_left_x_offset_from_centre + rec2.width;
	int rec2_bottom_right_y = patch_centre_y - rec2.top_left_y_offset_from_centre + rec2.height;
	
	double rec2_sum = integral_image.at<float>(rec2_bottom_right_x, rec2_bottom_right_y) 
						- integral_image.at<float>(rec2_bottom_right_x, rec2_bottom_right_y - rec2.height)
						- integral_image.at<float>(rec2_bottom_right_x - rec2.width, rec2_bottom_right_y)
						+ integral_image.at<float>(rec2_bottom_right_x - rec2.width, rec2_bottom_right_y - rec2.height);

	
	double f1 = rec1_sum/((double)(rec1.height*rec1.width));
	double f2 = rec2_sum/((double)(rec2.height*rec2.width));
	
	return abs(f1 - f2);
}

DRRForest::DRRForest(int number_of_trees, int max_depth, path& gt_pose_dir, path& depth_dir, path& mask_dir)
						:number_of_trees(number_of_trees), max_depth(max_depth), functor_cache(vector<DRRFunctor>()) 
{
	PatchGenerator pg = PatchGenerator(gt_pose_dir, depth_dir, mask_dir, 100, 100, 10);
	train(pg);
}

Mat DRRForest::createIntegralImage(Mat& depth_image, int type){
	Mat integral_image = Mat(depth_image.rows, depth_image.cols, CV_32FC1);
	
//	Mat integral_image = Mat(depth_image);
	
	depth_image.copyTo(integral_image);
	
	for(int i = 0; i < depth_image.rows; i++){
		integral_image.at<float>(i, 0) = depth_image.at<float>(i, 0);
		integral_image.at<float>(0, i) = depth_image.at<float>(0, i);
	}
	
	//TODO:for now just do the depth channel
	for(int i = 1; i < integral_image.rows; i++){
		for(int j = 1; j < integral_image.cols; j++){
			//II(x,y) = image(x,y) + II(x-1, y) + II (x, y-1) - II(x-1, y-1)
			integral_image.at<float>(i,j) = depth_image.at<float>(i, j) 
											+ integral_image.at<float>(i, j-1) 
											+ integral_image.at<float>(i -1, j)
											- integral_image.at<float>(i-1, j-1);
		}
	}
	return Mat();
}
boost::shared_ptr<img_matrix> DRRForest::createImageCache(PatchGenerator& pg){
	unordered_map<string, int> depth_file_map = pg.getDepthFileKeys();
	int number_of_depth_images = pg.getNumberOfDetphFiles();
	boost::shared_ptr<img_matrix> image_cache = boost::shared_ptr<img_matrix>(new img_matrix(number_of_depth_images, max_channel_number + 1));
	
	
	for(unordered_map<string, int>::iterator it = depth_file_map.begin(); it != depth_file_map.end(); it++){
		Mat depth_img =  loadDepthImageCompressed(it->first);
		for(int i = 0; i <= max_channel_number; i++){
			image_cache->operator ()(it->second, i) = createIntegralImage(depth_img, i);
		}
	}
	
	return image_cache;
}


void DRRForest::train(PatchGenerator& pg){
	vector<ImagePatch> training_patches = pg.getTrainingPatches();

	boost::shared_ptr<img_matrix> image_cache = createImageCache(pg);
	vector<DRRFunctor> functors = generate_functors(500, pg.getPatchHeight(), pg.getPatchWidth(), 40, 40, image_cache);
	vector<vector<ImagePatch> > training_patch_splits = split_training_data_points(training_patches, number_of_trees);
	
	DRRClassSplitCalculator datasplit_calculator = DRRClassSplitCalculator();
	for(int i = 0; i < number_of_trees; i++){
		cout << "****************building tree " << i+1 << " of " << number_of_trees << "********************" << endl;
		vector<ImagePatch> patches_split = training_patch_splits.at(i);
		RandomTree<ImagePatch, DRRFunctor, DRRClassSplitCalculator, DiscriminativeRandomRegressionNode> tree(patches_split, functors, 100, 100, 15, datasplit_calculator);
		trees.push_back(tree);
	}
}




Rectangle DRRForest::generateRandomRectangle(int patch_height, int patch_width, int max_rec_height, int max_rec_width){
	int rec_height = get_random_number_in_range(0, max_rec_height);
	int rec_width = get_random_number_in_range(0, max_rec_width);
	
	int top_left_x_offset_from_centre = get_random_number_in_range(-1*(patch_width/2), (patch_width/2) - rec_width);
	int top_left_y_offset_from_centre = get_random_number_in_range(-1*(patch_height/2), (patch_height/2) - rec_height);
	
	Rectangle rec = Rectangle(top_left_x_offset_from_centre, top_left_y_offset_from_centre, rec_height, rec_width);
	return rec;
}

int DRRForest::getRandomChannel(){
	return get_random_number_in_range(0, DRRForest::max_channel_number);
}

std::vector<DRRFunctor> DRRForest::generate_functors(int number_of_functors, int image_patch_height, 
														int image_patch_width, int max_rec_height, 
														int max_rec_width, boost::shared_ptr<img_matrix> image_cache){
	//image cache has to be a vector of integral_image caches i.e. a vector of Mat arrays, where each index to the vector represents a channel
	//so access is cache[channel][image_key]
	vector<DRRFunctor> functors;
	
	functor_cache.resize(number_of_functors);

	int functor_id = 0;
	for(int i = 0; i < number_of_functors; i++){
		Rectangle rec1 = generateRandomRectangle(image_patch_height, image_patch_width, max_rec_height, max_rec_width);
		Rectangle rec2 = generateRandomRectangle(image_patch_height, image_patch_width, max_rec_height, max_rec_width);
		int channel = getRandomChannel();
		DRRFunctor f = DRRFunctor(image_cache,drr_rectangle_function, functor_id, rec1, rec2, channel);
		functor_cache[functor_id] = f;
		functors.push_back(f);
		functor_id++;
	}
	
	return functors;
}


//takes in a copy of training_data_points (note no &) so the get_and_remove will only effect local copy
vector<vector<ImagePatch> > DRRForest::split_training_data_points(vector<ImagePatch> training_patches, int number_of_splits){
	vector<vector<ImagePatch> > splits;

	int points_to_remove_per_split = training_patches.size() * 0.25;
	//so we have 75% of items in each one

	for(int i = 0; i < number_of_splits; i++){
		vector<ImagePatch> split = vector<ImagePatch>(training_patches);
		for(int j = 0; j < points_to_remove_per_split; j++){
			get_and_remove_random_item(split);
		}
		splits.push_back(split);
	}

	return splits;
}


DRRForest::~DRRForest() {
	// TODO Auto-generated destructor stub
}

