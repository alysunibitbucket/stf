/*
 * DiscriminativeRandomRegressionNode.h
 *
 *  Created on: 18 Dec 2012
 *      Author: aly
 */

#ifndef DISCRIMINATIVERANDOMREGRESSIONNODE_H_
#define DISCRIMINATIVERANDOMREGRESSIONNODE_H_
#include <vector>
#include "ImagePatch.h"
#include <boost/shared_ptr.hpp>
#include <opencv2/opencv.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>


typedef boost::numeric::ublas::matrix<cv::Mat> img_matrix;

struct Rectangle{
	int top_left_x_offset_from_centre;
	int top_left_y_offset_from_centre;
	int height;
	int width;
	
	Rectangle(int tlx_from_centre, int tly_from_centre, int h, int w):top_left_x_offset_from_centre(tlx_from_centre), top_left_y_offset_from_centre(tly_from_centre), height(h), width(w){};
};


struct DRRFunction{
   typedef double (*function_ptr)(const int& patch_centre_x, const int& patch_centre_y, 
		   	   	   	   	   	   	   Rectangle& rec1, Rectangle& rec2, const int& channel, const int& image_hash_key,
		   	   	   	   	   	   	   boost::shared_ptr<img_matrix> image_cache);
};

struct DRRFunctor{

private:
	boost::shared_ptr<img_matrix> image_cache;
	DRRFunction::function_ptr function;
	int* random_values;	
	int functor_id;
	
	
public:
	DRRFunctor(){};
	DRRFunctor(boost::shared_ptr<img_matrix> cache, DRRFunction::function_ptr function,int functor_id, Rectangle rec1, Rectangle rec2, int channel)
	:image_cache(cache), function(function), random_values(random_values), functor_id(functor_id)
	{}
	
	int get_functor_id(){
		return functor_id;
	}
	
	void set_functor_id(int f_id){
		functor_id = f_id;
	}

	double operator()(ImagePatch& data_point){
		
		if(image_cache == 0){
			std::cerr << "Trying to call function without valid image cache" << std::endl;
			exit(-1);
		}
		return 0;
//		return this->operator ()(data_point.get_patch_top_left_row(), data_point.get_patch_top_left_col(), data_point.get_image_hash_key());
	}
	
	//This version has been put in for test time for speed, during training the generic random tree class uses the version
	//above
//	double operator()(int patch_top_left_row, int patch_top_left_col, int image_hash_key){
//		if(image_cache == 0){
//			std::cerr << "Trying to call function without valid image cache" << std::endl;
//			exit(-1);
//		}
//		
//		
//		return function(patch_top_left_row, patch_top_left_col, image_hash_key, image_cache, random_values);
//	}
	
	void set_image_cache(boost::shared_ptr<img_matrix> cache ){
		image_cache = cache;	
	}
	
	void replace_image_cache(boost::shared_ptr<img_matrix> cache){
		image_cache.reset();
		set_image_cache(cache);
	}
	
	boost::shared_ptr<img_matrix> get_image_cache(){
		return image_cache;
	}
};

class DiscriminativeRandomRegressionNode {

private:
	boost::shared_ptr<DiscriminativeRandomRegressionNode> left;
	boost::shared_ptr<DiscriminativeRandomRegressionNode> right;
	double threshold;
	int functor_id;
	bool is_a_leaf;
public:
	DiscriminativeRandomRegressionNode();

	void mark_as_leaf(){
		is_a_leaf = true;
	}
	
	bool is_leaf(){
		return is_a_leaf;
	}
	
	void set_data(std::vector<ImagePatch>& data)
	{}
	
	
	void set_functor(DRRFunctor& functor){
		functor_id = functor.get_functor_id();
	}
	void set_threshold(double threshold){
		this->threshold = threshold;
	}
	
	void create_left_child(){
		left = boost::shared_ptr<DiscriminativeRandomRegressionNode>(new DiscriminativeRandomRegressionNode());
		
	}
	
	void create_right_child(){
		right = boost::shared_ptr<DiscriminativeRandomRegressionNode>(new DiscriminativeRandomRegressionNode());
	}
	
	boost::shared_ptr<DiscriminativeRandomRegressionNode> get_left_child(){
		return left;
	}
	
	boost::shared_ptr<DiscriminativeRandomRegressionNode> get_right_child(){
		return right;
	}
	
	~DiscriminativeRandomRegressionNode(){};
};

#endif /* DISCRIMINATIVERANDOMREGRESSIONNODE_H_ */
